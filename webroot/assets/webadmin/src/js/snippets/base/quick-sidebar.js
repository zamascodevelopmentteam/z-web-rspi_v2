var mQuickSidebar = function() {
    var topbarAside = $('#m_quick_sidebar');
    var topbarAsideTabs = $('#m_quick_sidebar_tabs');    
    var topbarAsideClose = $('#m_quick_sidebar_close');
    var topbarAsideToggle = $('#m_quick_sidebar_toggle');
    var topbarAsideContent = topbarAside.find('.m-quick-sidebar__content');
    var logs = $('#m_quick_sidebar_tabs_logs');
    var logsLists = logs.find(".m-list-timeline__items");
    var initLogs = function() {
        // init dropdown tabbable content
       

        if (logs.length === 0) {
            return;
        }

        var init = function() {
            var height = mUtil.getViewPort().height - topbarAsideTabs.outerHeight(true) - 60;

            // init settings scrollable content
            logs.css('height', height);
            mApp.initScroller(logs, {});
        }

        init();

        // reinit on window resize
        mUtil.addResizeHandler(init);
    }

    var initOffcanvasTabs = function() {
        initLogs();
    }

    var ajaxLogs = function(e){
        $.ajax({
            url : e,
            dataType : 'json',
            type : 'get',
            beforeSend : function(){
                mApp.block(topbarAside);
            },
            success : function(result){
                if(result.code == 200){
                    var html = '';
                    var a = 0;
                    $.each(result.auditLogs,function(index,item){
                        if(a == 0){
                            var classes = "m-list-timeline__badge--success";
                        }else if(a == 1){
                            var classes = "m-list-timeline__badge--danger";
                        }else if(a == 2){
                            var classes = "m-list-timeline__badge--warning";
                        }else if(a == 3){
                            var classes = "m-list-timeline__badge--info";
                            a = 0;
                        }
                        html +=  '<div class="m-list-timeline__item">'
                                +'<span class="m-list-timeline__badge '+classes+'"></span>'
                                +'<a href="#" class="m-list-timeline__text">'
                                    +item.description+' by id: '+item.id
                                +'</a>'
                                +'<span class="m-list-timeline__time">'
                                    +item.time
                                +'</span>'
                                +'</div>';
                        a = a+1;
                    })
                    
                    logsLists.html(html);
                    mApp.unblock(topbarAside);
                    topbarAsideContent.removeClass('m--hide');
                    initOffcanvasTabs();
                }
            },
            error : function(){
                mApp.unblock(topbarAside);
                swal("Ooopp!!!","Failed to get activity record","error");
                topbarAsideClose.click();
            }
        })
    }

    var initOffcanvas = function(url) {
        topbarAside.mOffcanvas({
            class: 'm-quick-sidebar',
            overlay: true,  
            close: topbarAsideClose,
            toggle: topbarAsideToggle
        });   

        // run once on first time dropdown shown
        topbarAside.mOffcanvas().on('afterShow', function() {
            ajaxLogs(url)
        });
    }

    return {     
        init: function(url) {  
            if (topbarAside.length === 0) {
                return;
            }

            initOffcanvas(url); 
        }
    };
}();

$(document).ready(function() {
    if(urlActivities.length != 0){
        mQuickSidebar.init(urlActivities);
    }
});