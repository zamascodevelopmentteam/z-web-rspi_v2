var Utils = function(){
    var arrayMonth = function(){
        var month = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
        return month;
    }
    var dateIndonesia = function(date, month = true,time = false){
        $output = "";
        if(date != null){
            var newDate = new Date(date)
            var tahun = newDate.getYear();
            tahun = (tahun < 1000 ) ? tahun + 1900 : tahun;
            var bulan = newDate.getMonth() + 1;
            var tanggal  = newDate.getDate(); 
            if(month == false){
                var output = tanggal + "-" + bulan + "-" + tahun;
            }else{
                bulan = arrayMonth()[bulan];
                var output = tanggal + " " + bulan + " " + tahun;
            }
            if(time == true){
                var jam = newDate.getHours();
                var minutes = newDate.getMinutes();
                output = output + " " + jam + ":" + minutes;
            }
        }
        
        return output;
    };
    var numberInputFormat = function(){
        $(".onlyNumber,input[textnumber='true']").number( true, 2 );
        $(".onlyNumberWithoutComa,input[textnumberWithoutComa='true']").number( true, 0 );
        $(".onlyNumberWithoutSeparator,input[textnumberWithoutSeparator='true']").number( true, 0,'');
    }
    var datePickerInput = function(){
        $("#date-time-picker,.date-time-picker,input[datetimepicker='true']").datetimepicker({todayHighlight:!0,autoclose:!0,pickerPosition:"bottom-left",format:"dd-mm-yyyy hh:ii"})
        $("#date-picker,.date-picker,input[datepicker='true']").datepicker({
            todayHighlight:!0,orientation:"bottom left",templates:{
                leftArrow:'<i class="la la-angle-left"></i>',rightArrow:'<i class="la la-angle-right"></i>'
            },
            autoclose : true,
            format : "dd-mm-yyyy"
        })
    }
    
    var loadGallery = function(source){
        $("body").addClass('show-gallery');
        var thisUrl = source.attr("href");
        var thisType = source.data("type");
        var thisCaption = source.data("caption");
        var thisDocImage = source.data("docUrl");
        $(".overlay-gallery__info h3").html(thisCaption);
        if(thisType == "image"){
            $(".image-viewer img").hide();
            $(".overlay-gallery__wrapper").html("<div class='image-viewer'><img src='"+thisUrl+"'></div>");
            var imageLoaded = function(){
                var imgH = $(".image-viewer img").height();
                var imgW = $(".image-viewer img").width();
                var bodH = $('body').height();
                var bodW = $('body').width();
                
                if(imgH > imgW){
                    $(".image-viewer").addClass('potrait');
                }else{
                    $(".image-viewer").addClass('landscape');
                }
                
                $(".image-viewer img").fadeIn();
            }
            $(".image-viewer img").each(function() {
                var tmpImg = new Image() ;
                tmpImg.onload = imageLoaded ;
                tmpImg.src = $(this).attr('src') ;
                
            });
        }else{
            $(".overlay-gallery__wrapper").html("<div class=\"pdf-viewer\"><img src='"+thisDocImage+"'><a href='"+thisUrl+"'>DOWNLOAD FILE</a></div>")
        }
    }

    var galleryShow = function(){
        
        $("body").on("click",".list-folder-zmc .viewFile",function(e){
            e.preventDefault();
            loadGallery($(this));
        })
        $("body").on("keyup",function(e){
            if(e.keyCode == 27 && $("body").hasClass('show-gallery')){
                $("body").removeClass('show-gallery');
                $(".overlay-gallery__wrapper").html('');
                $(".overlay-gallery__wrapper").removeClass('potrait');
                $(".overlay-gallery__wrapper").removeClass('landscape');
                $(".overlay-gallery__wrapper").attr('style','');
                $(".image-viewer").attr('style','');
            }
        })
    } 
    return{
        dateIndonesia : function(date, month = true,time = false){
            return dateIndonesia(date,month,time);
        },
        init : function(){
            numberInputFormat();
            datePickerInput();
            galleryShow();
        }
    }
}();
$(document).ready(function(){
    Utils.init();
})