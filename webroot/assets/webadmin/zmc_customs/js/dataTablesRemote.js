var DatatableRemoteAjax = function() {
    var t = function(urlData,columnData) {
        var t = $(".m_datatable").mDatatable({
                data: {
                    type: "remote",
                    source: {
                        read: {
                            url: urlData
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                layout: {
                    theme: "default",
                    class: "",
                    scroll: false,
                    footer: false
                },
                sortable: true,
                filterable: true,
                pagination: true,
                columns: columnData,
                  translate: {
                    records: {
                        processing: 'Tunggu sebentar...',
                        noRecords: 'Data tidak ditemukan'
                    },
                    toolbar: {
                        pagination: {
                            items: {
                                info: ''
                            }
                        }
                    }
                }
            }),
            e = t.getDataSourceQuery();
        $("#m_form_search").on("keyup", function(e) {
            var a = t.getDataSourceQuery();
            if(e.keyCode == 13){
                a.generalSearch = $(this).val().toLowerCase(); t.setDataSourceQuery(a);
                t.load();
            }
            if($(this).val() == ""){
                a.generalSearch = $(this).val().toLowerCase(); t.setDataSourceQuery(a);
                t.load();
            }
        }).val(e.generalSearch)
        $("body").on("click",".btn-delete-on-table",function(e){
            e.preventDefault();
            var thisUrl = $(this).attr("href");
            swal({
                title: 'Are you sure?',
                text: 'Please make sure if you want to delete this record',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, keep it'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url : thisUrl,
                        type : "delete",
                        dataType : "json",
                        beforeSend : function(){
                            swal('Please Wait', 'Requesting Data', 'info')
                        },
                        success : function(result){
                            if(result.code == 200){
                                swal(
                                    'Success',
                                    result.message,
                                    'success'
                                )
                                var a = t.getDataSourceQuery(); t.load();
                            }else if(result.code == 99){
                                swal("Ooopp!!!", result.message,"error");
                            }else{
                                swal("Ooopp!!!", result.message,"error");
                            }
                        },
                        error : function()
                        {
                            swal("Ooopp!!!","Failed to deleted record, please try again","error");
                        }
                    })
                // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                } else if (result.dismiss === 'cancel') {
                    
                }
            })
        });
        $('body').on('show.bs.dropdown','.m-datatable__table', function () {
            $('.m-datatable__table').css( "overflow", "inherit" );
       });
       $('body').on('hide.bs.dropdown','.m-datatable__table', function () {
           $('.m-datatable__table').css( "overflow", "auto" );
      });
    };
    return {
        init: function(urlData,columnData) {
            t(urlData,columnData)
        }
    }
}();
