<?php
namespace App\Test\TestCase\Controller\Webadmin;

use App\Controller\Webadmin\InspectionPackagesDetailsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Webadmin\InspectionPackagesDetailsController Test Case
 */
class InspectionPackagesDetailsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.inspection_packages_details',
        'app.inspection_packages',
        'app.inspection_parameters',
        'app.inspection_parameters_categories'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
