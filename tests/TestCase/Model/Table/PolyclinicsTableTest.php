<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PolyclinicsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PolyclinicsTable Test Case
 */
class PolyclinicsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PolyclinicsTable
     */
    public $Polyclinics;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.polyclinics',
        'app.doctors',
        'app.doctor_schedules'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Polyclinics') ? [] : ['className' => PolyclinicsTable::class];
        $this->Polyclinics = TableRegistry::get('Polyclinics', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Polyclinics);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
