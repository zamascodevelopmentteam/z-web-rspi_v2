<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AlbumsFilesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AlbumsFilesTable Test Case
 */
class AlbumsFilesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AlbumsFilesTable
     */
    public $AlbumsFiles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.albums_files',
        'app.albums'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AlbumsFiles') ? [] : ['className' => AlbumsFilesTable::class];
        $this->AlbumsFiles = TableRegistry::get('AlbumsFiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AlbumsFiles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
