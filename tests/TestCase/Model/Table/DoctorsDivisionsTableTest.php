<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DoctorsDivisionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DoctorsDivisionsTable Test Case
 */
class DoctorsDivisionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DoctorsDivisionsTable
     */
    public $DoctorsDivisions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.doctors_divisions',
        'app.doctors',
        'app.polyclinics',
        'app.doctor_schedules'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DoctorsDivisions') ? [] : ['className' => DoctorsDivisionsTable::class];
        $this->DoctorsDivisions = TableRegistry::get('DoctorsDivisions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DoctorsDivisions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
