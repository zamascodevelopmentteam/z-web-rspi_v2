<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LinksMapsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LinksMapsTable Test Case
 */
class LinksMapsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LinksMapsTable
     */
    public $LinksMaps;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.links_maps',
        'app.links'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LinksMaps') ? [] : ['className' => LinksMapsTable::class];
        $this->LinksMaps = TableRegistry::get('LinksMaps', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LinksMaps);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
