<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConflictInterestsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConflictInterestsTable Test Case
 */
class ConflictInterestsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConflictInterestsTable
     */
    public $ConflictInterests;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.conflict_interests'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ConflictInterests') ? [] : ['className' => ConflictInterestsTable::class];
        $this->ConflictInterests = TableRegistry::get('ConflictInterests', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConflictInterests);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
