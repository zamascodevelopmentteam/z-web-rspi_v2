<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContentsCategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContentsCategoriesTable Test Case
 */
class ContentsCategoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContentsCategoriesTable
     */
    public $ContentsCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contents_categories',
        'app.contents',
        'app.links'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ContentsCategories') ? [] : ['className' => ContentsCategoriesTable::class];
        $this->ContentsCategories = TableRegistry::get('ContentsCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContentsCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
