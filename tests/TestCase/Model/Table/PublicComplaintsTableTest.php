<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PublicComplaintsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PublicComplaintsTable Test Case
 */
class PublicComplaintsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PublicComplaintsTable
     */
    public $PublicComplaints;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.public_complaints'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PublicComplaints') ? [] : ['className' => PublicComplaintsTable::class];
        $this->PublicComplaints = TableRegistry::get('PublicComplaints', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PublicComplaints);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
