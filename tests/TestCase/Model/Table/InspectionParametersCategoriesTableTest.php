<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InspectionParametersCategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InspectionParametersCategoriesTable Test Case
 */
class InspectionParametersCategoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InspectionParametersCategoriesTable
     */
    public $InspectionParametersCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.inspection_parameters_categories',
        'app.inspection_parameters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InspectionParametersCategories') ? [] : ['className' => InspectionParametersCategoriesTable::class];
        $this->InspectionParametersCategories = TableRegistry::get('InspectionParametersCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InspectionParametersCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
