<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WebSettings Model
 *
 * @method \App\Model\Entity\WebSetting get($primaryKey, $options = [])
 * @method \App\Model\Entity\WebSetting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WebSetting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WebSetting|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WebSetting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WebSetting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WebSetting findOrCreate($search, callable $callback = null, $options = [])
 */
class WebSettingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('web_settings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('AuditStash.AuditLog');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('Web_Name')
            ->maxLength('Web_Name', 200,'Invalid max length')
            ->requirePresence('Web_Name', 'create')
            ->notEmpty('Web_Name');
            
        $validator
                ->scalar('Web_Logo_Width')
                ->integer('Web_Logo_Width','Only number allowed')
                ->requirePresence('Web_Logo_Width', 'create')
                ->notEmpty('Web_Logo_Width');
        
        $validator
                ->scalar('Web_Logo_Height')
                ->integer('Web_Logo_Height','Only number allowed')
                ->requirePresence('Web_Logo_Height', 'create')
                ->notEmpty('Web_Logo_Height');

        $validator
                ->requirePresence('Web_Logo', 'create')
                ->add('Web_Logo', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'on' => function ($context) {
                    return !empty($context['data']['Web_Logo']);
                },
                'message'=>'Only JPEG and PNG allowed'])
                ->allowEmpty('Web_Logo');
        
                
        $validator
            ->scalar('Web_Description')
            ->requirePresence('Web_Description', 'create')
            ->notEmpty('Web_Description');
            
        $validator
            ->scalar('Web_FirstPage')
            ->requirePresence('Web_FirstPage', 'create')
            ->notEmpty('Web_FirstPage');

        
        $validator
            ->scalar('Web_Copyright')
            ->requirePresence('Web_Copyright', 'create')
            ->notEmpty('Web_Copyright');

        $validator
            ->requirePresence('Web_Favico', 'create')
            ->add('Web_Favico', 'file', [
            'rule' => ['mimeType', ['image/jpeg', 'image/png', 'image/x-icon']],
            'on' => function ($context) {
                return !empty($context['data']['Web_Favico']);
            },
            'message'=>'Only JPEG, ICON and PNG allowed'])
            ->allowEmpty('Web_Favico');
            
        return $validator;
    }
}
