<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PublicComplaints Model
 *
 * @method \App\Model\Entity\PublicComplaint get($primaryKey, $options = [])
 * @method \App\Model\Entity\PublicComplaint newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PublicComplaint[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PublicComplaint|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PublicComplaint patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PublicComplaint[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PublicComplaint findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PublicComplaintsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('public_complaints');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'file' => [
                'fields' => [
                    'dir' => 'file_dir',
                ],
                'path' => 'webroot{DS}assets{DS}public_complaints{DS}{year}{month}{DS}files{DS}',
                'keepFilesOnDelete' => false
            ]
        ]);
        // $this->addBehavior('AuditStash.AuditLog');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('reporter_type')
            ->requirePresence('reporter_type', 'create')
            ->notEmpty('reporter_type');

        $validator
            ->scalar('work_unit')
            ->maxLength('work_unit', 225)
            ->allowEmpty('work_unit');

        $validator
            ->scalar('position')
            ->maxLength('position', 225)
            ->allowEmpty('position');

        $validator
            ->scalar('letter_of_assignment')
            ->maxLength('letter_of_assignment', 225)
            ->allowEmpty('letter_of_assignment');

        $validator
            ->scalar('name')
            ->maxLength('name', 99)
            ->allowEmpty('name');

        $validator
            ->scalar('address')
            ->allowEmpty('address');

        $validator
            ->scalar('job')
            ->maxLength('job', 99)
            ->allowEmpty('job');

        $validator
            ->scalar('city')
            ->maxLength('city', 99)
            ->allowEmpty('city');

        $validator
            ->scalar('province')
            ->maxLength('province', 99)
            ->allowEmpty('province');

        $validator
            ->scalar('origin')
            ->maxLength('origin', 99)
            ->allowEmpty('origin');

        $validator
            ->scalar('nip')
            ->maxLength('nip', 25)
            ->allowEmpty('nip');

        $validator
            ->scalar('message')
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->allowEmpty('file');

        $validator
            ->scalar('file_dir')
            ->maxLength('file_dir', 225)
            ->allowEmpty('file_dir');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        return $validator;
    }
}
