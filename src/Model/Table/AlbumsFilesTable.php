<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Inflector;
use App\Model\Behavior\Imagine as Imagine;

/**
 * AlbumsFiles Model
 *
 * @property \App\Model\Table\AlbumsTable|\Cake\ORM\Association\BelongsTo $Albums
 *
 * @method \App\Model\Entity\AlbumsFile get($primaryKey, $options = [])
 * @method \App\Model\Entity\AlbumsFile newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AlbumsFile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AlbumsFile|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AlbumsFile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AlbumsFile[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AlbumsFile findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AlbumsFilesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('albums_files');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Albums', [
            'foreignKey' => 'album_id',
            'joinType' => 'INNER'
        ]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'file' => [
                'fields' => [
                    'dir' => 'file_dir',
                    'type' => 'file_type',
                ],
                'path' => 'webroot{DS}assets{DS}albums{DS}{field-value:folder_parent}',
                'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $extension = strtolower($extension);
                    if(in_array($extension,['jpg','png','jpeg'])){
                        $extension;
                        $source     = $data['tmp_name'];
                        $width      = 150;
                        $height     = 150;
                        $imagineComponent->gdImageCropAndSave($source, $tmp,['width'=>$width,'height'=>$height]);
                        return [
                            $data['tmp_name'] => $data['name'],
                            $tmp => 'thumbnail-' . $data['name'],
                        ];
                    }else{
                        return [
                            $data['tmp_name'] => $data['name'],
                        ];
                    }

                    
                },
                'keepFilesOnDelete' => false
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('file')
            ->maxLength('file', 225)
            ->requirePresence('file', 'create')
            ->notEmpty('file');

        $validator
            ->scalar('file_dir')
            ->maxLength('file_dir', 225)
            ->allowEmpty('file_dir');

        $validator
            ->scalar('file_type')
            ->maxLength('file_type', 225)
            ->allowEmpty('file_type');

        $validator
            ->boolean('showable')
            ->allowEmpty('showable');

        $validator
            ->boolean('downloadable')
            ->allowEmpty('downloadable');

        $validator
            ->scalar('title')
            ->maxLength('title', 225)
            ->allowEmpty('title');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        $validator
            ->integer('created_by')
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['album_id'], 'Albums'));

        return $rules;
    }

    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options)
    {

        $album = $this->Albums->get($data['album_id']);
        $data['folder_parent'] = $this->getParentNodeAlbum($album).$album->folder;
        return true;
    }

    public function getParentNodeAlbum($entity){
        if(!empty($entity->parent_id)){
            $parent = $this->Albums->find('path',['for'=>$entity->parent_id]);
            $dir = "";
            foreach($parent as $key => $r){
                $title = Inflector::camelize(strtolower($r->title));
                $folder = $r->folder;
                $dir .= $folder.DS; 
            }
            return $dir;
        }else{
            return "";
        }
        
    }
}
