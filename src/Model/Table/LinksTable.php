<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Model\Behavior\Imagine as Imagine;

/**
 * Links Model
 *
 * @property \App\Model\Table\LinksMapsTable|\Cake\ORM\Association\BelongsTo $LinksMaps
 * @property \App\Model\Table\ContentsTable|\Cake\ORM\Association\BelongsTo $Contents
 *
 * @method \App\Model\Entity\Link get($primaryKey, $options = [])
 * @method \App\Model\Entity\Link newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Link[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Link|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Link patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Link[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Link findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LinksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('links');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Tree', [
            'level' => 'level', 
        ]);

        $this->belongsTo('LinksMaps', [
            'foreignKey' => 'links_map_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Contents', [
            'foreignKey' => 'content_id',
            'joinType' => 'INNER'
        ]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'picture' => [
                'fields' => [
                    'dir' => 'picture_dir',
                ],
                'path' => 'webroot{DS}assets{DS}link{DS}{year}{month}{DS}picture{DS}',
                'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 300;
                    $height     = 300;
                    $imagineComponent->gdImageCropAndSave($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumbnail-' . $data['name'],
                    ];
                },
                'keepFilesOnDelete' => false
            ]
        ]);
        $this->addBehavior('AuditStash.AuditLog');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 225)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('_type')
            ->requirePresence('_type', 'create')
            ->notEmpty('_type');

        $validator
            ->scalar('target')
            ->requirePresence('target', 'create')
            ->notEmpty('target');

        $validator
            ->add('content_id', 'CompareType', [
                'rule' => ['compareWith', '_type'],
                'message' => __('This field is required'),
                'on' => function ($context) {
                    if($context['data']['_type'] == "Contents" && $context['data']['content_id'] != ""){
                        return false;
                    }elseif($context['data']['_type'] == "Contents" && $context['data']['content_id'] == ""){
                        return true;
                    }
                    return true;
                }
            ])
            ->allowEmpty('content_id');

        $validator
            ->scalar('url')
            ->maxLength('url', 225)
            ->requirePresence('url', 'create')
            ->notEmpty('url');



        $validator
            ->add('picture', 'file', [
            'rule' => ['mimeType', ['image/jpeg', 'image/png']],
            'on' => function ($context) {
                return !empty($context['data']['picture']);
            },
            'message'=>'Only JPEG and PNG allowed'])
            ->allowEmpty('picture');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['links_map_id'], 'LinksMaps'));
        $rules->add($rules->existsIn(['content_id'], 'Contents'));

        return $rules;
    }
}
