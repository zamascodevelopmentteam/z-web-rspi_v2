<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Model\Behavior\Imagine as Imagine;
/**
 * Contents Model
 *
 * @property \App\Model\Table\ContentsCategoriesTable|\Cake\ORM\Association\BelongsTo $ContentsCategories
 * @property \App\Model\Table\LinksTable|\Cake\ORM\Association\HasMany $Links
 *
 * @method \App\Model\Entity\Content get($primaryKey, $options = [])
 * @method \App\Model\Entity\Content newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Content[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Content|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Content patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Content[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Content findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ContentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contents');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ContentsCategories', [
            'foreignKey' => 'contents_category_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Links', [
            'foreignKey' => 'content_id'
        ]);
        $this->hasMany('RelatedContents', [
            'foreignKey' => 'created_by',
            'bindingKey' => 'created_by',
            'className' => 'Contents',
        ]);
        $this->belongsTo('CreatedUsers', [
            'foreignKey' => 'created_by',
            'className'=>'Users'
        ]);
        $this->belongsTo('ModifiedUsers', [
            'foreignKey' => 'modified_by',
            'className'=>'Users'
        ]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'picture' => [
                'fields' => [
                    'dir' => 'picture_dir',
                ],
                'path' => 'webroot{DS}assets{DS}content{DS}{year}{month}{DS}picture{DS}',
                'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 600;
                    $height     = 600;
                    $imagineComponent->gdImageCropAndSave($source, $tmp,['width'=>$width,'height'=>$height]);
                    
                    $tmp_2        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 600;
                    $height     = 600;
                    $imagineComponent->resize($source, $tmp_2,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumbnail-' . $data['name'],
                        $tmp_2 => 'resize-' . $data['name'],
                    ];
                },
                'keepFilesOnDelete' => false
            ]
        ]);
        $this->addBehavior('AuditStash.AuditLog');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

            $validator
            ->scalar('title')
            ->maxLength('title', 225)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('contents_category_id')
            ->integer('id')
            ->requirePresence('contents_category_id', 'create')
            ->notEmpty('contents_category_id');

        $validator
            ->add('picture', 'file', [
            'rule' => ['mimeType', ['image/jpeg', 'image/png', 'image/jpg']],
            'on' => function ($context) {
                return !empty($context['data']['picture']);
            },
            'message'=>'Only JPEG,JPG and PNG allowed'])
            ->allowEmpty('picture');

        $validator
            ->scalar('body')
            ->requirePresence('body', 'create')
            ->allowEmpty('body');

        $validator
            ->integer('sort')
            ->requirePresence('sort', 'create')
            ->allowEmpty('sort');
        
            return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['contents_category_id'], 'ContentsCategories'));

        return $rules;
    }
}
