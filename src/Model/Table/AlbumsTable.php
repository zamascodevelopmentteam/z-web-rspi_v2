<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Inflector;
use Cake\Filesystem\Folder;

/**
 * Albums Model
 *
 * @property \App\Model\Table\AlbumsTable|\Cake\ORM\Association\BelongsTo $ParentAlbums
 * @property \App\Model\Table\AlbumsTable|\Cake\ORM\Association\HasMany $ChildAlbums
 * @property \App\Model\Table\AlbumsFilesTable|\Cake\ORM\Association\HasMany $AlbumsFiles
 *
 * @method \App\Model\Entity\Album get($primaryKey, $options = [])
 * @method \App\Model\Entity\Album newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Album[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Album|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Album patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Album[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Album findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class AlbumsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('albums');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree', [
            'level' => 'level', 
        ]);

        $this->belongsTo('ParentAlbums', [
            'className' => 'Albums',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildAlbums', [
            'className' => 'Albums',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('AlbumsFiles', [
            'foreignKey' => 'album_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->addBehavior('AuditStash.AuditLog');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 225)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->boolean('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {

        return $rules;
    }

    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        $entity->slug = Inflector::slug($entity->title);   
        if($entity->isNew()){
            $entity->folder = rand(100,99999);   
        }

        return true;
    }

    public function afterDelete(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options){
        $folder = $entity->folder;
        $folderParent = $this->getParentNodeAlbum($entity);
        $dir = new Folder(WWW_ROOT.'assets'.DS.'albums'.DS.$folderParent.$folder);
        $dir->delete();
    }

    public function afterSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        if($entity->isNew()){
            if(!empty($entity->title)){
                $title = Inflector::camelize(strtolower($entity->title));
                $folder = $entity->folder;
                $dir = new Folder();
                $folderParent = $this->getParentNodeAlbum($entity);
                $dir->create(WWW_ROOT.'assets'.DS.'albums'.DS.$folderParent.$folder, 0777);
            }
        }else{
            if($entity->getOriginal('parent_id') != $entity->parent_id){
                $folder = Inflector::camelize(strtolower($entity->getOriginal('folder')));
                $folder_old = $folder;
                $folder_parent_old = $this->getParentNodeAlbum((object)$entity->extractOriginal($entity->visibleProperties()));
                
                $title = Inflector::camelize(strtolower($entity->title));
                $folder_new = $entity->folder;
                $folder_parent_new = $this->getParentNodeAlbum($entity);

                $folder = new Folder(WWW_ROOT.'assets'.DS.'albums'.DS.$folder_parent_old.$folder_old);
                $folder->copy([
                    'to' => WWW_ROOT.'assets'.DS.'albums'.DS.$folder_parent_new.$folder_new,
                    'from' => WWW_ROOT.'assets'.DS.'albums'.DS.$folder_parent_old.$folder_old, // Will cause a cd() to occur
                    'mode' => 0777,
                    'scheme' => Folder::OVERWRITE
                ]);
                $folder->delete();
                $this->AlbumsFiles->updateAll([
                    'file_dir' => 'webroot'.DS.'assets'.DS.'albums'.DS.$folder_parent_new.$folder_new,
                    'folder_parent' => $folder_parent_new.$folder_new
                ],[
                    'album_id' => $entity->id
                ]);

            }
        }
        
        

        return true;
    }
    
    public function getParentNodeAlbum($entity){
        if(!empty($entity->parent_id)){
            $parent = $this->find('path',['for'=>$entity->parent_id]);
            $dir = "";
            foreach($parent as $key => $r){
                $title = Inflector::camelize(strtolower($r->title));
                $folder = $r->folder;
                $dir .= $folder.DS; 
            }
            return $dir;
        }else{
            return "";
        }
        
    }
}
