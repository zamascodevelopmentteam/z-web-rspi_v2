<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Polyclinics Model
 *
 * @property \App\Model\Table\DoctorsTable|\Cake\ORM\Association\HasMany $Doctors
 *
 * @method \App\Model\Entity\Polyclinic get($primaryKey, $options = [])
 * @method \App\Model\Entity\Polyclinic newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Polyclinic[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Polyclinic|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Polyclinic patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Polyclinic[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Polyclinic findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PolyclinicsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('polyclinics');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('polyclinic')
            ->maxLength('polyclinic', 225)
            ->requirePresence('polyclinic', 'create')
            ->notEmpty('polyclinic');

        return $validator;
    }
}
