<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DoctorsDivision Entity
 *
 * @property int $id
 * @property int $doctor_id
 * @property int $polyclinic_id
 * @property \Cake\I18n\FrozenTime $created
 * @property int $created_at
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $modified_by
 *
 * @property \App\Model\Entity\Doctor $doctor
 * @property \App\Model\Entity\Polyclinic $polyclinic
 */
class DoctorsDivision extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'doctor_id' => true,
        'polyclinic_id' => true,
        'created' => true,
        'created_at' => true,
        'modified' => true,
        'modified_by' => true,
        'doctor' => true,
        'polyclinic' => true
    ];
}
