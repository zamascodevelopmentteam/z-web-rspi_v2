<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PublicComplaint Entity
 *
 * @property int $id
 * @property string $reporter_type
 * @property string $work_unit
 * @property string $position
 * @property string $letter_of_assignment
 * @property string $name
 * @property string $address
 * @property string $job
 * @property string $city
 * @property string $province
 * @property string $origin
 * @property string $nip
 * @property string $message
 * @property $file
 * @property string $file_dir
 * @property bool $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $modified_by
 */
class PublicComplaint extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'reporter_type' => true,
        'position' => true,
        'name' => true,
        'address' => true,
        'job' => true,
        'city' => true,
        'province' => true,
        'origin' => true,
        'nip' => true,
        'message' => true,
        'file' => true,
        'file_dir' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'modified_by' => true
    ];
}
