<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Link Entity
 *
 * @property int $id
 * @property string $title
 * @property int $links_map_id
 * @property string $type
 * @property string $target
 * @property string $url
 * @property int $content_id
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\LinksMap $links_map
 * @property \App\Model\Entity\Content $content
 */
class Link extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'links_map_id' => true,
        '_type' => true,
        'target' => true,
        'url' => true,
        'content_id' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'links_map' => true,
        'content' => true,
        'parent_id' => true,
        'lft' => true,
        'rght' => true,
        'meta_description' => true,
        'meta_keyword' => true,
        'level' => true,
        'sort' => true,
        'picture' => true,
        'picture_dir' => true,
        'status' => true
    ];
}
