<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConflictInterest Entity
 *
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $message
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class ConflictInterest extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'name' => true,
        'address' => true,
        'phone' => true,
        'message' => true,
        'created' => true,
        'modified' => true
    ];
}
