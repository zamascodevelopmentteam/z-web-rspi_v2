<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AlbumsFile Entity
 *
 * @property int $id
 * @property int $album_id
 * @property string $file
 * @property string $file_dir
 * @property string $file_type
 * @property bool $showable
 * @property bool $downloadable
 * @property string $title
 * @property string $description
 * @property bool $status
 * @property \Cake\I18n\FrozenTime $created
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $modified_by
 *
 * @property \App\Model\Entity\Album $album
 */
class AlbumsFile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'album_id' => true,
        'file' => true,
        'folder_parent' => true,
        'file_dir' => true,
        'file_type' => true,
        'showable' => true,
        'downloadable' => true,
        'title' => true,
        'description' => true,
        'status' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'album' => true
    ];
}
