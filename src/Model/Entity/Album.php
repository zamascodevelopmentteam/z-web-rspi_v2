<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Album Entity
 *
 * @property int $id
 * @property int $parent_id
 * @property string $title
 * @property string $slug
 * @property bool $status
 * @property int $lft
 * @property int $rght
 * @property int $level
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\ParentAlbum $parent_album
 * @property \App\Model\Entity\ChildAlbum[] $child_albums
 * @property \App\Model\Entity\AlbumsFile[] $albums_files
 */
class Album extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'parent_id' => true,
        'title' => true,
        'folder' => true,
        'slug' => true,
        'status' => true,
        'lft' => true,
        'rght' => true,
        'level' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'parent_album' => true,
        'child_albums' => true,
        'albums_files' => true
    ];
}
