<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Content Entity
 *
 * @property int $id
 * @property int $contents_category_id
 * @property string $title
 * @property string $slug
 * @property string $picture
 * @property string $picture_dir
 * @property string $cover
 * @property string $cover_dir
 * @property string $body
 * @property string $meta_keyword
 * @property string $meta_description
 * @property bool $status
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $created
 * @property int $modified_by
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\ContentsCategory $contents_category
 * @property \App\Model\Entity\Link[] $links
 */
class Content extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'contents_category_id' => true,
        'title' => true,
        'slug' => true,
        'picture' => true,
        'picture_dir' => true,
        'cover' => true,
        'cover_dir' => true,
        'body' => true,
        'sort' => true,
        'meta_keyword' => true,
        'meta_description' => true,
        'status' => true,
        'created_by' => true,
        'created' => true,
        'modified_by' => true,
        'modified' => true,
        'contents_category' => true,
        'links' => true,
    ];
}
