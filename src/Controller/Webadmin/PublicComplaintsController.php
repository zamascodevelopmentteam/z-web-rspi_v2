<?php
namespace App\Controller\WebAdmin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * PublicComplaints Controller
 *
 * @property \App\Model\Table\PublicComplaintsTable $PublicComplaints
 *
 * @method \App\Model\Entity\PublicComplaint[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PublicComplaintsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->PublicComplaints;
            $searchAble = [
                'PublicComplaints.reporter_type'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'PublicComplaints.id',
                'defaultSort' => 'desc'
            ];
            $baseData   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $baseData['data'];
            $meta = $baseData['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "Whistle Blowing System";
            $titlesubModule = "List ".$titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Public Complaint id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $publicComplaint = $this->PublicComplaints->get($id, [
            'contain' => []
        ]);

        $this->set('publicComplaint', $publicComplaint);
        $titleModule = "Whistle Blowing System";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    
}
