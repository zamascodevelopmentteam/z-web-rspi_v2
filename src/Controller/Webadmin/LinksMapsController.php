<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * LinksMaps Controller
 *
 * @property \App\Model\Table\LinksMapsTable $LinksMaps
 *
 * @method \App\Model\Entity\LinksMap[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LinksMapsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->LinksMaps;
            $searchAble = [
                'LinksMaps.id',
                'LinksMaps.name'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'LinksMaps.id',
                'defaultSort' => 'desc',
                'defaultSearch' => [
                    // [    
                    //     'keyField' => 'group_id',
                    //     'condition' => '=',
                    //     'value' => 1
                    // ]
                ],
                    
            ];
            $asd   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $asd['data'];
            $meta = $asd['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "Sitemap";
            $titlesubModule = "List Sitemap";
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Links Map id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $linksMap = $this->LinksMaps->get($id, [
            'contain' => ['Links']
        ]);

        $this->set('linksMap', $linksMap);
        $titleModule = "Sitemap";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $linksMap = $this->LinksMaps->newEntity();
        if ($this->request->is('post')) {
            $linksMap = $this->LinksMaps->patchEntity($linksMap, $this->request->getData());
            if ($this->LinksMaps->save($linksMap)) {
                $this->Flash->success(__('The links map has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The links map could not be saved. Please, try again.'));
        }
        $this->set(compact('linksMap'));
        $titleModule = "Sitemap";
        $titlesubModule = "Add ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Links Map id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $linksMap = $this->LinksMaps->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $linksMap = $this->LinksMaps->patchEntity($linksMap, $this->request->getData());
            if ($this->LinksMaps->save($linksMap)) {
                $this->Flash->success(__('The links map has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The links map could not be saved. Please, try again.'));
        }
        $this->set(compact('linksMap'));

        $titleModule = "Sitemap";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Links Map id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $linksMap = $this->LinksMaps->get($id);
        if ($this->LinksMaps->delete($linksMap)) {
            $code = 200;
            $message = __('The sitemap has been deleted.');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('The sitemap could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
