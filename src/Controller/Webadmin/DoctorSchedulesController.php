<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * DoctorSchedules Controller
 *
 * @property \App\Model\Table\DoctorSchedulesTable $DoctorSchedules
 *
 * @method \App\Model\Entity\DoctorSchedule[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DoctorSchedulesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      $this->loadModel('DoctorsDivisions');
      if($this->request->is('ajax')){
          $source = $this->DoctorSchedules;

          $searchAble = [
              'DoctorSchedules.name',
              'DoctorSchedules.polyclinic'
          ];
          $data = [
              'source'=>$source,
              'searchAble' => $searchAble,
              'defaultField' => 'DoctorSchedules.id',
              'defaultSort' => 'desc',
              'contain' => ['DoctorsDivisions.Doctors', 'DoctorsDivisions.Polyclinics']

          ];
          $baseData   = $this->Datatables->make($data);
          //$this->set('data', $asd);
          $data = $baseData['data'];
          $meta = $baseData['meta'];
          $this->set('data',$data);
          $this->set('meta',$meta);
          $this->set('_serialize',['data','meta']);
      }else{
          $titleModule = "Jadwal Praktek";
          $titlesubModule = "List ".$titleModule;
          $breadCrumbs = [
              Router::url(['action' => 'index']) => $titlesubModule
          ];
          $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
      }
    }

    /**
     * View method
     *
     * @param string|null $id Doctor Schedule id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $doctorSchedule = $this->DoctorSchedules->get($id, [
            'contain' => ['Doctors']
        ]);

        $this->set('doctorSchedule', $doctorSchedule);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('DoctorsDivisions');
        $doctorSchedule = $this->DoctorSchedules->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $doctorSchedule = $this->DoctorSchedules->patchEntity($doctorSchedule, $data);
            // dd($data);
            // die();
            if ($this->DoctorSchedules->save($doctorSchedule)) {
                $this->Flash->success(__('The doctor schedule has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The doctor schedule could not be saved. Please, try again.'));
        }
        $doctors = $this->DoctorsDivisions->find('all')
          ->contain([
              'Doctors', 'Polyclinics'
          ])
          ->select([
              'name' => 'CONCAT(Doctors.name," - ", Polyclinics.polyclinic)',
              'id' => 'DoctorsDivisions.id'
          ]);

        $titleModule = "Jadwal Praktek";
        $titlesubModule = "Add ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));

        $this->set(compact('doctorSchedule', 'doctors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Doctor Schedule id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $doctorSchedule = $this->DoctorSchedules->get($id, [
            'contain' => ['Doctors']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $doctorSchedule = $this->DoctorSchedules->patchEntity($doctorSchedule, $data);
            if ($this->DoctorSchedules->save($doctorSchedule)) {
                $this->Flash->success(__('The doctor schedule has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The doctor schedule could not be saved. Please, try again.'));
        }
        $doctors = $this->DoctorSchedules->Doctors->find('list', ['limit' => 200]);
        $this->set(compact('doctorSchedule', 'doctors'));

        $titleModule = "Jadwal Praktek";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Doctor Schedule id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $doctorSchedule = $this->DoctorSchedules->get($id);
       if ($this->DoctorSchedules->delete($doctorSchedule)) {
            $code = 200;
            $message = __('Data Kegiatan Vendor berhasil di hapus.');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('Data Kegiatan Vendor gagal di hapus. Silahkan ulangi kembali.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
