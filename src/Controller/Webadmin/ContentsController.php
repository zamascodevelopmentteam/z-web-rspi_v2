<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\Utility\Inflector;
/**
 * Contents Controller
 *
 * @property \App\Model\Table\ContentsTable $Contents
 *
 * @method \App\Model\Entity\Content[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->Contents;
            $searchAble = [
                'Contents.title',
                'ContentsCategories.title',
                'Contents.slug'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'Contents.id',
                'defaultSort' => 'desc',
                'contain' =>[
                    'ContentsCategories'
                ]
                    
            ];
            $baseData   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $baseData['data'];
            $meta = $baseData['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "Content";
            $titlesubModule = "List ".$titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $content = $this->Contents->get($id, [
            'contain' => ['ContentsCategories']
        ]);

        $this->set('content', $content);
        $titleModule = "Content";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(
                compact( 
                'titleModule',
                'breadCrumbs',
                'titlesubModule',
                'content'
            ));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $content = $this->Contents->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $content = $this->Contents->patchEntity($content, $this->request->getData());
            $content->slug = Inflector::slug(strtolower($content->title));
            if ($this->Contents->save($content)) {
                $message = __('The content has been saved.');
                $code    = 200;
                $validate = "";
            }else{
                $message = __('The content could not be saved. Please, try again.');
                $code    = 50;
                $validate = $content->errors();
            }
            $this->set(compact('message','code','validate'));
            $this->set('_serialize',['message','code','validate']);
        }
        $contentsCategories = $this->Contents->ContentsCategories->find('list', ['limit' => 200,'conditions'=>['status'=>1]]);
        $titleModule = "Content";
        $titlesubModule = "Add ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('content','contentsCategories','titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $content = $this->Contents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $content = $this->Contents->patchEntity($content, $this->request->getData());
            $content->slug = Inflector::slug(strtolower($content->title));
            if ($this->Contents->save($content)) {
                $message = __('The content has been saved.');
                $code    = 200;
            }else{
                $message = __('The content could not be saved. Please, try again.');
                $code    = 50;
            }
            $this->set(compact('message','code'));
            $this->set('_serialize',['message','code']);
        }
        $contentsCategories = $this->Contents->ContentsCategories->find('list', ['limit' => 200,'conditions'=>[
            'OR'=>
                [
                    'status'=>1,
                    'id'=>$content->contents_category_id
                ]
            ]
        ]);
        $titleModule = "Content";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact( 'titleModule',
                            'breadCrumbs',
                            'titlesubModule',
                            'content',
                            'contentsCategories'
                        ));
    }

    /**
     * Delete method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $content = $this->Contents->get($id);
        if ($this->Contents->delete($content)) {
            $message = __('The group has been deleted.');
            $status = 'success';
            $code = 200;
        } else {
            $code = 99;
            $message = __('The group could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
        //return $this->redirect(['action' => 'index']);
    }

    

}
