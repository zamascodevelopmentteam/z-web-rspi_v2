<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\Query;
/**
 * ContentsCategories Controller
 *
 * @property \App\Model\Table\ContentsCategoriesTable $ContentsCategories
 *
 * @method \App\Model\Entity\ContentsCategory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContentsCategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
 public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->ContentsCategories;

            $searchAble = [
                'ContentsCategories.title',
                'ContentsCategories.slug'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'ContentsCategories.slug',
                'defaultSort' => 'desc',
                    
            ];
            $baseData   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $baseData['data'];
            $meta = $baseData['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "Content";
            $titlesubModule = "List ".$titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Contents Category id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contentsCategory = $this->ContentsCategories->get($id);

        $this->set('contentsCategory', $contentsCategory);
        $titleModule = "Content Category";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('contentsCategory','titleModule','titlesubModule','breadCrumbs'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contentsCategory = $this->ContentsCategories->newEntity();
        if ($this->request->is('post')) {
            $contentsCategory = $this->ContentsCategories->patchEntity($contentsCategory, $this->request->getData());
            if ($this->ContentsCategories->save($contentsCategory)) {
                $this->Flash->success(__('The contents category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contents category could not be saved. Please, try again.'));
        }
        $titleModule = "Content Category";
        $titlesubModule = "Add ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('contentsCategory','titleModule','titlesubModule','breadCrumbs'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Contents Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contentsCategory = $this->ContentsCategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contentsCategory = $this->ContentsCategories->patchEntity($contentsCategory, $this->request->getData());
            if ($this->ContentsCategories->save($contentsCategory)) {
                $this->Flash->success(__('The contents category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contents category could not be saved. Please, try again.'));
        }
        $titleModule = "Content Category";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact( 'titleModule',
                            'breadCrumbs',
                            'titlesubModule',
                            'contentsCategory'
                        ));
        $this->set(compact('contentsCategory'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Contents Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contentsCategory = $this->ContentsCategories->get($id);
        if ($this->ContentsCategories->delete($contentsCategory)) {
            $message = __('The group has been deleted.');
            $status = 'success';
            $code = 200;
        } else {
            $code = 99;
            $message = __('The group could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }

    }
}
