<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;
use App\Model\Behavior\Imagine as Imagine;

/**
 * Doctors Controller
 *
 * @property \App\Model\Table\DoctorsTable $Doctors
 *
 * @method \App\Model\Entity\Doctor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DoctorsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      if($this->request->is('ajax')){
          $source = $this->Doctors;

          $searchAble = [
              'Doctors.name',
          ];
          $data = [
              'source'=>$source,
              'searchAble' => $searchAble,
              'defaultField' => 'Doctors.id',
              'defaultSort' => 'desc',
              'contain' => [
              ]

          ];
          $baseData   = $this->Datatables->make($data);
          //$this->set('data', $asd);
          $data = $baseData['data'];
          $meta = $baseData['meta'];
          $this->set('data',$data);
          $this->set('meta',$meta);
          $this->set('_serialize',['data','meta']);
      }else{
          $titleModule = "Dokter";
          $titlesubModule = "List ".$titleModule;
          $breadCrumbs = [
              Router::url(['action' => 'index']) => $titlesubModule
          ];
          $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
      }
    }

    /**
     * View method
     *
     * @param string|null $id Doctor id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $doctor = $this->Doctors->get($id, [
            'contain' => ['DoctorSchedules']
        ]);

        $titleModule = "Dokter";
        $titlesubModule = "Detail ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));

        $this->set('doctor', $doctor);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $doctor = $this->Doctors->newEntity();
        if ($this->request->is('post')) {
            $doctor = $this->Doctors->patchEntity($doctor, $this->request->getData());
            if ($this->Doctors->save($doctor)) {
                $this->Flash->success(__('The doctor has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The doctor could not be saved. Please, try again.'));
        }
        $polyclinics = $this->Doctors->find('list', ['limit' => 200]);

        $titleModule = "Dokter";
        $titlesubModule = "Add ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule', 'titlesubModule', 'breadCrumbs'));
        
        $this->set(compact('doctor', 'polyclinics'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Doctor id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $doctor = $this->Doctors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $doctor = $this->Doctors->patchEntity($doctor, $this->request->getData());
            if ($this->Doctors->save($doctor)) {
                $this->Flash->success(__('The doctor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The doctor could not be saved. Please, try again.'));
        }

        $titleModule = "Dokter";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','doctor'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Doctor id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $doctor = $this->Doctors->get($id);
        if ($this->Doctors->delete($doctor)) {
            $code = 200;
            $message = __('Dokter telah di hapus.');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('Dokter gagal di hapus, silahkan ulangi kembali.');
            $status = 'error';
        }

        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
