<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * Albums Controller
 *
 * @property \App\Model\Table\AlbumsTable $Albums
 *
 * @method \App\Model\Entity\Album[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AlbumsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        if(php_sapi_name() !== 'cli'){
            $this->Auth->allow(['getFolder','uploadFiles','deleteFile','delete']);
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $parent_id =$this->request->query('parent_id');
            $parent_id = trim($parent_id);
            if($parent_id == null || empty($parent_id)){
                $xdata = $this->Albums->find('all',[
                    'conditions' => ['parent_id = 0']
                ])->select([
                    'id' => 'Albums.id',
                    'text' => 'Albums.title',
                    'icon' => '("fa fa-folder icon-lg m--font-warning")',
                    'children' => '((SELECT COUNT(*) FROM albums AS a where a.parent_id = Albums.id) > 0)'
                ]);
                foreach($xdata as $key => $r){
                    if($r->children == '0'){
                        $r->children = false;
                    }else{
                        $r->children = true;
                    }
                }  
                $data[0] =[
                    'id' => 0,
                    'text' => 'ROOT',
                    'icon' => 'fa fa-folder icon-lg m--font-warning',
                    'children' => $xdata,
                    'type' => 'root',
                    'state'  => [
                        'opened' => true
                    ]
                ];
            }else{
                $data = $this->Albums->find('all',[
                    'conditions' => ['parent_id' => $parent_id]
                ])->select([
                    'id' => 'Albums.id',
                    'text' => 'Albums.title',
                    'icon' => '("fa fa-folder icon-lg m--font-warning")',
                    'children' => '((SELECT COUNT(*) FROM albums AS a where a.parent_id = Albums.id) > 0)'
                ]);
                foreach($data as $key => $r){
                    if($r->children == '0'){
                        $r->children = false;
                    }else{
                        $r->children = true;
                    }
                }    
            }
            
            $this->set('data',$data);
            $this->set('_serialize','data');
        }else{
            $titleModule = "Albums";
            $titlesubModule = "List ".$titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $album = $this->Albums->newEntity();
            $this->set(compact('titleModule','breadCrumbs','titlesubModule','album'));
        }
    }

    public function getFolder(){
        $parent_id = $this->request->query('parent_id');
        if($parent_id == ""){
            $conditions = ['parent_id = 0'];
        }else{
            $conditions = ['parent_id' => $parent_id];
        }
        $data = $this->Albums->find('all',[
            'conditions' => $conditions
        ])->select([
            'id' => 'Albums.id',
            'text' => 'Albums.title',
            'parent_id' => 'Albums.parent_id',
            'icon' => '("fa fa-folder icon-lg m--font-warning")',
        ]);
        $dataFile = [];
        if($parent_id != ""){
            $dataFile = $this->Albums->AlbumsFiles->find('all',[
                'conditions'=>['album_id' => $parent_id]
            ]);
        }
        $this->set('dataFolder',$data);
        $this->set('dataFile',$dataFile);
        $this->set('_serialize',['dataFolder','dataFile']);
    }

    /**
     * View method
     *
     * @param string|null $id Album id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $album = $this->Albums->get($id, [
            'contain' => ['ParentAlbums', 'ChildAlbums', 'AlbumsFiles']
        ]);

        $titleModule = "Album";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','album'));

        $this->set('album', $album);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $data = $this->request->getData();
        if(empty($data['parent_id'])){
            $data['parent_id'] = 0;
        }
        if(empty($data['id'])){
            $album = $this->Albums->newEntity();
        }else{
            $album = $this->Albums->get($data['id']);
        }
        
        if ($this->request->is('post')) {
            $album = $this->Albums->patchEntity($album, $data);
            if ($this->Albums->save($album)) {
                $message = __('The album has been created.');
                $status = 'success';
                $code = 200;
            }else {
                $code = 99;
                $message = __('The album could not be created. Please, try again.');
                $status = 'error';
            }
            if($this->request->is('ajax')){
                $this->set('album',$album);
                $this->set('code',$code);
                $this->set('message',$message);
                $this->set('_serialize',['code','message','album']);
            }else{
                $this->Flash->{$status}($message);
                return $this->redirect(['action' => 'index']);
            }
        }
        $parentAlbums = $this->Albums->ParentAlbums->find('list', ['limit' => 200]);

        $titleModule = "Album";
        $titlesubModule = "Add ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','album', 'parentAlbums'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Album id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function uploadFiles($album_id)
    {
        $albumFiles = $this->Albums->AlbumsFiles->newEntity();
        if($this->request->is(['post','put'])){
            $data = $this->request->getData();
            $data['album_id'] = $album_id;
            $albumFiles = $this->Albums->AlbumsFiles->patchEntity($albumFiles,$data,['validate' => false]);
            if ($this->Albums->AlbumsFiles->save($albumFiles)) {
                $message = __('The file uploaded.');
                $status = 'success';
                $code = 200;
            } else {
                $code = 99;
                $message = __('The file could not be uploaded. Please, try again.');
                $status = 'error';
            }
            $this->set('code',[$status => $code]);
            $this->set('_serialize','code');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Album id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $content = $this->Albums->get($id);
        if ($this->Albums->delete($content)) {
            $message = __('The album has been deleted.');
            $status = 'success';
            $code = 200;
        } else {
            $code = 99;
            $message = __('The album could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
    public function deleteFile($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $content = $this->Albums->AlbumsFiles->get($id);
        if ($this->Albums->AlbumsFiles->delete($content)) {
            $message = __('The file has been deleted.');
            $status = 'success';
            $code = 200;
        } else {
            $code = 99;
            $message = __('The file could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
