<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * DoctorsDivisions Controller
 *
 * @property \App\Model\Table\DoctorsDivisionsTable $DoctorsDivisions
 *
 * @method \App\Model\Entity\DoctorsDivision[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DoctorsDivisionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
          $source = $this->DoctorsDivisions;

          $searchAble = [
              'DoctorsDivisions.doctor_id',
              'DoctorsDivisions.polyclinic_id',
              'Doctors.name',
              'Polyclinics.polyclinic'
          ];
          $data = [
              'source'=>$source,
              'searchAble' => $searchAble,
              'defaultField' => 'DoctorsDivisions.id',
              'defaultSort' => 'desc',
              'contain' => ['Doctors','Polyclinics']

          ];
          $baseData   = $this->Datatables->make($data);
          //$this->set('data', $asd);
          $data = $baseData['data'];
          $meta = $baseData['meta'];
          $this->set('data',$data);
          $this->set('meta',$meta);
          $this->set('_serialize',['data','meta']);
      }else{
          $titleModule = "Divisi Dokter";
          $titlesubModule = "List ".$titleModule;
          $breadCrumbs = [
              Router::url(['action' => 'index']) => $titlesubModule
          ];
          $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
      }
    }

    /**
     * View method
     *
     * @param string|null $id Doctors Division id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $doctorsDivision = $this->DoctorsDivisions->get($id, [
            'contain' => ['Doctors', 'Polyclinics']
        ]);

        $this->set('doctorsDivision', $doctorsDivision);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $doctorsDivision = $this->DoctorsDivisions->newEntity();
        if ($this->request->is('post')) {
            $doctorsDivision = $this->DoctorsDivisions->patchEntity($doctorsDivision, $this->request->getData());
            if ($this->DoctorsDivisions->save($doctorsDivision)) {
                $this->Flash->success(__('The doctors division has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The doctors division could not be saved. Please, try again.'));
        }
        $doctors = $this->DoctorsDivisions->Doctors->find('list', ['limit' => 200]);
        $polyclinics = $this->DoctorsDivisions->Polyclinics->find('list', [
            'keyField' => 'id',
            'valueField' => 'polyclinic',
            'limit' => 200
        ]);
        $this->set(compact('doctorsDivision', 'doctors', 'polyclinics'));

        $titleModule = "Divisi Dokter";
        $titlesubModule = "Tambah ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Doctors Division id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $doctorsDivision = $this->DoctorsDivisions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $doctorsDivision = $this->DoctorsDivisions->patchEntity($doctorsDivision, $this->request->getData());
            if ($this->DoctorsDivisions->save($doctorsDivision)) {
                $this->Flash->success(__('The doctors division has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The doctors division could not be saved. Please, try again.'));
        }
        $doctors = $this->DoctorsDivisions->Doctors->find('list', ['limit' => 200]);
        $polyclinics = $this->DoctorsDivisions->Polyclinics->find('list', [
            'keyField' => 'id',
            'valueField' => 'polyclinic',
            'limit' => 200
        ]);
        $this->set(compact('doctorsDivision', 'doctors', 'polyclinics'));

        $titleModule = "Divisi Dokter";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Doctors Division id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $doctorsDivision = $this->DoctorsDivisions->get($id);
        if ($this->DoctorsDivisions->delete($doctorsDivision)) {
            $code = 200;
            $message = __('Data Divisi dokter berhasil di hapus.');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('Data Divisi dokter gagal di hapus. Silahkan ulangi kembali.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
