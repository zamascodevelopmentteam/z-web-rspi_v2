<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * Polyclinics Controller
 *
 * @property \App\Model\Table\PolyclinicsTable $Polyclinics
 *
 * @method \App\Model\Entity\Polyclinic[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PolyclinicsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      if($this->request->is('ajax')){
          $source = $this->Polyclinics;

          $searchAble = [
              'Polyclinics.polyclinic'
          ];
          $data = [
              'source'=>$source,
              'searchAble' => $searchAble,
              'defaultField' => 'Polyclinics.id',
              'defaultSort' => 'desc'

          ];
          $baseData   = $this->Datatables->make($data);
          //$this->set('data', $asd);
          $data = $baseData['data'];
          $meta = $baseData['meta'];
          $this->set('data',$data);
          $this->set('meta',$meta);
          $this->set('_serialize',['data','meta']);
      }else{
          $titleModule = "Poliklinik";
          $titlesubModule = "List ".$titleModule;
          $breadCrumbs = [
              Router::url(['action' => 'index']) => $titlesubModule
          ];
          $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
      }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $polyclinic = $this->Polyclinics->newEntity();
        if ($this->request->is('post')) {
            $polyclinic = $this->Polyclinics->patchEntity($polyclinic, $this->request->getData());
            if ($this->Polyclinics->save($polyclinic)) {
                $this->Flash->success(__('The polyclinic has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The polyclinic could not be saved. Please, try again.'));
        }

        $titleModule = "Poliklinik";
        $titlesubModule = "Add ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "Add ".$titleModule,
            Router::url(['action' => 'view']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        $this->set(compact('polyclinic'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Polyclinic id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $polyclinic = $this->Polyclinics->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $polyclinic = $this->Polyclinics->patchEntity($polyclinic, $this->request->getData());
            if ($this->Polyclinics->save($polyclinic)) {
                $this->Flash->success(__('The polyclinic has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The polyclinic could not be saved. Please, try again.'));
        }

        $titleModule = "Poliklinik";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "Edit ".$titleModule,
            Router::url(['action' => 'view']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        $this->set(compact('polyclinic'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Polyclinic id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $polyclinic = $this->Polyclinics->get($id);
        if ($this->Polyclinics->delete($polyclinic)) {
            $this->Flash->success(__('The polyclinic has been deleted.'));
        } else {
            $this->Flash->error(__('The polyclinic could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
