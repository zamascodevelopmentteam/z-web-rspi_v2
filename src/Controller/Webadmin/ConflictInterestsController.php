<?php
namespace App\Controller\WebAdmin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * ConflictInterests Controller
 *
 * @property \App\Model\Table\ConflictInterestsTable $ConflictInterests
 *
 * @method \App\Model\Entity\ConflictInterest[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConflictInterestsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->ConflictInterests;
            $searchAble = [
                'ConflictInterests.reporter_type'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'ConflictInterests.id',
                'defaultSort' => 'desc'
            ];
            $baseData   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $baseData['data'];
            $meta = $baseData['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "Kepentingan Benturan";
            $titlesubModule = "List ".$titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Conflict Interest id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $conflictInterest = $this->ConflictInterests->get($id, [
            'contain' => []
        ]);

        $this->set('conflictInterest', $conflictInterest);

        $titleModule = "List Kepentingan Benturan";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $conflictInterest = $this->ConflictInterests->newEntity();
        if ($this->request->is('post')) {
            $conflictInterest = $this->ConflictInterests->patchEntity($conflictInterest, $this->request->getData());
            if ($this->ConflictInterests->save($conflictInterest)) {
                $this->Flash->success(__('The conflict interest has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The conflict interest could not be saved. Please, try again.'));
        }
        $this->set(compact('conflictInterest'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Conflict Interest id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $conflictInterest = $this->ConflictInterests->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $conflictInterest = $this->ConflictInterests->patchEntity($conflictInterest, $this->request->getData());
            if ($this->ConflictInterests->save($conflictInterest)) {
                $this->Flash->success(__('The conflict interest has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The conflict interest could not be saved. Please, try again.'));
        }
        $this->set(compact('conflictInterest'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Conflict Interest id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $conflictInterest = $this->ConflictInterests->get($id);
        if ($this->ConflictInterests->delete($conflictInterest)) {
            $this->Flash->success(__('The conflict interest has been deleted.'));
        } else {
            $this->Flash->error(__('The conflict interest could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
