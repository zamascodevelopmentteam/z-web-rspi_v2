<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;

class ApisController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        if(php_sapi_name() !== 'cli'){
            $this->Auth->allow([
              'getLinkBySiteMap', 'getDoctors', 'getPolyclinics'
            ]);
        }
    }

    public function getLinkBySiteMap($id)
    {
        $this->loadModel('Links');
        $parents = $this->Links->find('treeList',[
            'conditions' => [
                'links_map_id' => $id
            ]
        ]);
        $this->set('parents',$parents);
        $this->set('_serialize','parents');
    }

    public function getDoctors($id)
    {
        $this->loadModel('Doctors');
        if($this->request->is('ajax')){
            $search = "";
            if($this->request->query('search')){
                $search = $this->request->query('search');
            }
            $data = $this->Doctors->find('all',[
                'conditions' => [
                    'id'=> $id
                ],
                'order' => [
                    'name' => 'ASC'
                ]
            ]);
            $results = [];
            $a =0;
            foreach($data as $key => $val){
                $results[$a]['id'] = $val->id;
                $results[$a]['text'] = $val->name;
                $a++;
            }
            $this->set(compact('results'));
            $this->set('_serialize',['results']);
        }
    }
    public function getPolyclinics($id)
    {
        $this->loadModel('Polyclinics');
        if($this->request->is('ajax')){
            $search = "";
            if($this->request->query('search')){
                $search = $this->request->query('search');
            }
            $data = $this->Polyclinics->find('all',[
                'conditions' => [
                    'id'=> $id
                ],
                'order' => [
                    'polyclinic' => 'ASC'
                ]
            ]);
            $results = [];
            $a =0;
            foreach($data as $key => $val){
                $results[$a]['id'] = $val->id;
                $results[$a]['text'] = $val->name;
                $a++;
            }
            $this->set(compact('results'));
            $this->set('_serialize',['results']);
        }
    }
}
