<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * Covers Controller
 *
 * @property \App\Model\Table\CoversTable $Covers
 *
 * @method \App\Model\Entity\Cover[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CoversController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->Covers;
            $searchAble = [
                'Covers.id',
                'Covers.name',
                'Covers.url',
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'Covers.id',
                'defaultSort' => 'desc',
                    
            ];
            $groups   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $groups['data'];
            $meta = $groups['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "Cover";
            $titlesubModule = "List ".$titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Cover id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cover = $this->Covers->get($id, [
            'contain' => []
        ]);

        $titleModule = "Cover";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','cover'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cover = $this->Covers->newEntity();
        if ($this->request->is('post')) {
            $cover = $this->Covers->patchEntity($cover, $this->request->getData());
            if ($this->Covers->save($cover)) {
                $this->Flash->success(__('The cover has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cover could not be saved. Please, try again.'));
        }
        $titleModule = "Cover";
        $titlesubModule = "Add ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','cover'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Cover id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cover = $this->Covers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cover = $this->Covers->patchEntity($cover, $this->request->getData());
            if ($this->Covers->save($cover)) {
                $this->Flash->success(__('The cover has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cover could not be saved. Please, try again.'));
        }
        $titleModule = "Cover";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','cover'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Cover id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cover = $this->Covers->get($id);
        if ($this->Covers->delete($cover)) {
            $this->Flash->success(__('The cover has been deleted.'));
        } else {
            $this->Flash->error(__('The cover could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
