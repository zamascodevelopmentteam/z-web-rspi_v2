<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * Links Controller
 *
 * @property \App\Model\Table\LinksTable $Links
 *
 * @method \App\Model\Entity\Link[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LinksController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->Links;
            $searchAble = [
                'Links.id',
                'Links._type',
                'Links.title',
                'Links.url',
                'LinksMaps.name'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'Links.id',
                'defaultSort' => 'desc',
                'defaultSearch' => [
                    // [    
                    //     'keyField' => 'group_id',
                    //     'condition' => '=',
                    //     'value' => 1
                    // ]
                ],
                'contain' => ['LinksMaps']
                    
            ];
            $asd   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $asd['data'];
            $meta = $asd['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "Link";
            $titlesubModule = "List Links";
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Link id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $link = $this->Links->get($id, [
            'contain' => ['LinksMaps']
        ]);

        $this->set('link', $link);
        $titleModule = "Link";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $link = $this->Links->newEntity();
        $linksMaps = $this->Links->LinksMaps->find('list', ['limit' => 200]);
        $contents = $this->Links->Contents->find('all',[
            'contain'=>['ContentsCategories'],
            'conditions' => [
                'OR' => [
                    'Contents.status' => 1
                ]
            ]
        ]);
        $parents = [];
        $listContents = [];
        $contentJson = [];
        foreach($contents as $key => $content){
            $listContents[$content->contents_category->title][$content->id] = $content->title;
            $contentJson[$content->id] = $content->slug; 
        }
        $contents = json_encode($contentJson);
        
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if($data['_type'] != "Contents"){
                $data['content_id'] = "";
            }else{
                if(!empty($data['content_id'])){
                    $getSlug = json_decode($contents);
                    $data['url'] = $getSlug->{$data['content_id']};
                }
            }
            $link = $this->Links->patchEntity($link, $data);
            if ($this->Links->save($link)) {
                $this->Flash->success(__('The link has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The link could not be saved. Please, try again.'));
            $parents = $this->Links->find('treeList',[
                'condition' => [
                    'links_map_id' => $link->links_map_id
                ]
            ]);
        }
        $this->set(compact('link', 'linksMaps', 'contents','listContents','parents'));
        $titleModule = "Link";
        $titlesubModule = "Add ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Link id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $link = $this->Links->get($id, [
            'contain' => []
        ]);
        $linksMaps = $this->Links->LinksMaps->find('list', ['limit' => 200]);
        $contents = $this->Links->Contents->find('all',[
            'contain'=>['ContentsCategories'],
            'conditions' => [
                'OR' => [
                    'Contents.id' => $link->content_id,
                    'Contents.status' => 1
                ]
            ]
        ]);
        
        $listContents = [];
        $contentJson = [];
        foreach($contents as $key => $content){
            $listContents[$content->contents_category->title][$content->id] = $content->title;
            $contentJson[$content->id] = $content->slug; 
        }
        $contents = json_encode($contentJson);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if($data['_type'] != "Contents"){
                $data['content_id'] = "";
            }else{
                if(!empty($data['content_id'])){
                    $getSlug = json_decode($contents);
                    $data['url'] = $getSlug->{$data['content_id']};
                }
            }
            $link = $this->Links->patchEntity($link, $data);
            if ($this->Links->save($link)) {
                $this->Flash->success(__('The link has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The link could not be saved. Please, try again.'));
        }
        $parents = $this->Links->find('treeList',[
            'condition' => [
                'links_map_id' => $link->links_map_id
            ]
        ]);
        $this->set(compact('link', 'linksMaps', 'contents','listContents','parents'));
        $titleModule = "Link";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Link id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $link = $this->Links->get($id);
        if ($this->Links->delete($link)) {
            $code = 200;
            $message = __('The link has been deleted.');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('The link could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
