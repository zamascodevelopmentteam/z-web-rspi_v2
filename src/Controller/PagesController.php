<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Utility\Inflector;
use Cake\Mailer\Email;

class PagesController extends AppController
{
    public function index()
    {
        $slug = $this->defaultAppSettings['Web.FirstPage'];
        $this->loadModel('Links');
        //checkLink//
        $link = $this->Links->find('all',[
            'conditions' => [
                'url' => $slug
            ]
        ])->first();
        if(!empty($link)){
            if($link->_type == "Contents"){
                $this->loadModel('Contents');
                $content = $this->Contents->find('all',[
                    'conditions' => [
                        'Contents.slug' => $slug,
                        'Contents.status' => 1
                    ]
                ]);
                if(empty($content)){
                    $this->render('/Error/not_found');
                }else{
                    $content = $content->first();
                    $this->set(compact('content'));
                    $this->render('display');
                }
            }else{
                $this->{$slug}();
            }
        }else{
            try {
                $this->{$slug}();
            } catch (Exception $e) {
                $this->render('/Error/not_found');
            }

        }
    }

    public function display()
    {
        $slug = $this->request->slug;
        if($this->request->_name == "pages"){
            $this->loadModel('Links');
            //checkLink//
            $link = $this->Links->find('all',[
                'conditions' => [
                    'url' => $slug
                ]
            ])->first();

            if($this->request->slug ="berita"){
                $this->loadModel('Contents');
                    $this->set(compact('berita', 'slug'));
            }

            if(!empty($link)){
                if($link->_type == "Contents"){
                    $this->loadModel('Contents');
                    $content = $this->Contents->find('all',[
                        'conditions' => [
                            'Contents.slug' => $slug,
                            'Contents.status' => 1
                        ]
                    ]);
                    if(empty($content)){
                        $this->render('/Error/not_found');
                    }else{
                        $content = $content->first();
                        $this->set(compact('content', 'berita'));
                    }
                }else{
                    $slug = Inflector::variable($slug);
                    $this->{$slug}();
                }
            }else{
                $slug = Inflector::variable($slug);
                try {
                    $this->{$slug}();
                } catch (Exception $e) {
                    $this->render('/Error/not_found');
                }
            }
        }elseif($this->request->_name == "customPages"){
            $this->loadModel('Contents');
            $content = $this->Contents->find('all',[
                'conditions' => [
                    'Contents.slug' => $slug,
                    'Contents.status' => 1
                ]
            ]);
            if($this->request->category == "berita"){
                $this->loadModel('Contents');

                $berita = $this->Contents->find('all',[
                'contain' => [
                    'ContentsCategories'
                ],
                ])
                ->order(['Contents.created' =>'DESC'])
                ->where([
                    'ContentsCategories.id = 2'
                ])
                ->limit('5')
                ;

            }else if($this->request->category == "artikel"){
                $this->loadModel('Contents');

                $berita = $this->Contents->find('all',[
                'contain' => [
                    'ContentsCategories'
                ],
                ])
                ->order(['Contents.created' =>'DESC'])
                ->where([
                    'ContentsCategories.id = 3'
                ])
                ;
            }

            if(empty($content)){
                $this->render('/Error/not_found');
            }else{
                $content = $content->first();
                $this->set(compact('content', 'berita'));
            }
        }
    }

    public function beranda()
    {
        $this->loadModel('Contents');
        $layanans = $this->Contents->find('all',[
            'contain' => [
                'ContentsCategories'
            ],
            'conditions' => [
                'Contents.status' => 1,
                'ContentsCategories.slug' => 'layanan'
            ],
            'limit' => 4
        ]);
        $artikels = $this->Contents->find('all',[
            'contain' => [
                'ContentsCategories'
            ],
            'conditions' => [
                'Contents.status' => 1,
                'AND' => [
                    'OR' => [
                        'ContentsCategories.slug' => 'artikel',
                        'OR' => [
                            'ContentsCategories.slug' => 'berita',
                        ]
                    ]
                ]
            ],
            'limit' => 4
        ])->order(['Contents.created' => 'DESC']);
        $sertifikats = $this->Contents->find('all',[
            'contain' => [
                'ContentsCategories'
            ],
            'conditions' => [
                'Contents.status' => 1,
                'ContentsCategories.slug' => 'sertifikasi'
            ]
        ])->order('Contents.sort ASC');
        $this->set(compact('layanans','artikels','beritas','sertifikats'));
        $this->render('home');
    }

    public function jadwalDokter(){
      $this->loadModel('Doctors');
      $this->loadModel('DoctorSchedules');
      $this->loadModel('DoctorsDivisions');
      $this->loadModel('Polyclinics');

      $jadwal = $this->DoctorSchedules->find('all', [
        'contain' => [
          'DoctorsDivisions.Doctors', 'DoctorsDivisions.Polyclinics'
        ]
      ]);

      $poli = $this->Polyclinics->find('all');

      $this->set(compact('jadwal', 'poli'));
      $this->render('jadwalDokter');
    }

    public function tarif(){
        $this->loadModel('Rates');
        $rates = $this->Rates->find('threaded',[
            'conditions' => [
                'Rates.status' => '1'
            ]
        ]);
        $this->set(compact('rates'));

        $this->render('tarif');
    }

    public function berita()
    {
        $category = "Berita";
        $conditions = [];
        if($this->request->isPost()){
            return $this->redirect([
                'controller' => 'Pages',
                'action' => 'display',
                'slug' => 'berita',
                '?'=>[
                    'view' => 'search',
                    'search' => $this->request->getData('search')
                ]
            ]);
        }
        if(!empty($this->request->query('search'))){
            $conditions = [
                'AND' => [
                    'OR' => [
                        'Contents.title LIKE' => "%".$this->request->query('search')."%",
                        'Contents.body LIKE' => "%".$this->request->query('search')."%",
                    ]
                ]
            ];
            $category = "Pencarian Berita";
        }
        $this->loadModel('Contents');
        $artikels = $this->Contents->find('all',[
            'contain' => [
                'ContentsCategories'
            ],
            'conditions' => [
                'Contents.status' => 1,
                'ContentsCategories.slug' => 'berita',
                $conditions
            ]
        ])->order(['Contents.created' => 'DESC']);

        $this->set(compact('artikels','category'));
        $this->render('daftar-berita');
    }

    public function artikel()
    {
        $conditions = [];
        $category = "Artikel";
        if($this->request->isPost()){
            return $this->redirect([
                'controller' => 'Pages',
                'action' => 'display',
                'slug' => 'artikel',
                '?'=>[
                    'view' => 'search',
                    'search' => $this->request->getData('search')
                ]
            ]);
        }
        if(!empty($this->request->query('search'))){
            $conditions = [
                'AND' => [
                    'OR' => [
                        'Contents.title LIKE' => "%".$this->request->query('search')."%",
                        'Contents.body LIKE' => "%".$this->request->query('search')."%",
                    ]
                ]
            ];
            $category = "Pencarian Artikel";
        }
        $this->loadModel('Contents');
        $artikels = $this->Contents->find('all',[
            'contain' => [
                'ContentsCategories'
            ],
            'conditions' => [
                'Contents.status' => 1,
                'ContentsCategories.slug' => 'artikel',
                $conditions
            ]
        ])->order(['Contents.created' => 'DESC']);
        $this->set(compact('artikels','category'));
        $this->render('daftar-berita');
    }

    public function hubungiKami()
    {
        $this->loadModel('ContactUs');
        $contactUs = $this->ContactUs->newEntity();
        if ($this->request->is('post')) {
            $contactUs = $this->ContactUs->patchEntity($contactUs, $this->request->getData());
            if ($this->ContactUs->save($contactUs)) {
                $this->Flash->success(__('Pesan telah terkirim.'));

                return $this->redirect($this->request->here);
            }
            $this->Flash->error(__('Pesan gagal terkirim silahkan ulangi kembali.'));
        }
        $this->set(compact('contactUs'));
        $this->render('hubungi_kami');
    }

    public function wbs()
    {
        $this->loadModel('PublicComplaints');
        $publicComplaint = $this->PublicComplaints->newEntity();
        if ($this->request->is('post')) {
            $publicComplaint = $this->PublicComplaints->patchEntity($publicComplaint, $this->request->getData());
            if ($this->PublicComplaints->save($publicComplaint)) {
                $this->Flash->success(__('Pengaduan telah terkirim.'));
                $email = new Email('default');
                $email->from(['mytestemailfordev@gmail.com' => 'WEBSITE BBLK SURABAYA'])
                ->template('wbs')
                ->emailFormat('html')
                ->viewVars(['data' => $publicComplaint])
                //->attachments($pdf)
                ->to('wbsbblks@gmail.com')
                ->subject('[WBS] Seseorang mengirimkan laporan');
                if(!empty($publicComplaint->file)){
                    $email->attachments([
                        $publicComplaint->file =>['file'=>ROOT.DS.$publicComplaint->file_dir.$publicComplaint->file],
                    ]);
                }
                $email->send();

                return $this->redirect($this->request->here);
            }
            $this->Flash->error(__('Pengaduan gagal terkirim silahkan ulangi kembali.'));
        }
        $this->set(compact('publicComplaint'));
        $this->render('wbs');
    }

    public function conflict()
    {
        $this->loadModel('ConflictInterests');
        $conflictInterest = $this->ConflictInterests->newEntity();
        if ($this->request->is('post')) {
            $conflictInterest = $this->ConflictInterests->patchEntity($conflictInterest, $this->request->getData());
            if ($this->ConflictInterests->save($conflictInterest)) {
                $this->Flash->success(__('Pengaduan Benturan Kepentingan telah terkirim.'));
                $email = new Email('default');
                $email->from(['mytestemailfordev@gmail.com' => 'WEBSITE BBLK SURABAYA'])
                ->template('conflict')
                ->emailFormat('html')
                ->viewVars(['data' => $conflictInterest])
                //->attachments($pdf)
                ->to('wbsbblks@gmail.com')
                ->subject('[BENTURAN KEPENTINGAN] Seseorang mengirimkan laporan');
                $email->send();


                return $this->redirect($this->request->here);
            }
            $this->Flash->error(__('Pengaduan gagal terkirim silahkan ulangi kembali.'));
        }
        $this->set(compact('conflictInterest'));
        $this->render('conflict');
    }
    public function wbk()
    {

        $this->render('wbk');
    }

    public function albums()
    {
        $this->loadModel('Albums');
        $album = $this->Albums->find('all',[
            'conditions' => [
                'slug' => $this->request->slug
            ],
            'contain'=>[
                'AlbumsFiles',
                'ChildAlbums'
            ]
        ])->first();
        $this->set(compact(['album']));
        $this->render('album');
    }
}
