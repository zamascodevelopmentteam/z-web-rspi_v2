<?php
    $this->assign('title', 'Whistle Blowing System / WBS');
    $this->start('breadcrumb');
        echo '<li class="breadcrumb-item"><a href="'.$this->request->base.'/"><i class="fa fa-home"></i> Beranda</a></li>';
        echo '<li class="breadcrumb-item active">WBK</li>';
    $this->end();
?>
<section class="container wrapper">
    <section class="row">
        <section class="col-md-12">
            <header class="title-wrapper text-left">
                <div class="main-title">
                    <h1>WBK</h1>
                </div>
            </header>
        </section>
    </section>
    <section class="row">
        <section class="col-md-12">
            <section class="body-wrapper">
                <div class="row">
                    <div class="col-md-4">
                        <a href="<?=$this->request->base;?>/wbs" class="widget-icon-item">
                            <figure>
                                <div class="image">
                                    <?php $image = $this->Url->build('/assets/img/wbk/thumbnail-asdfg.png');?>
                                    <img src="<?=$image;?>">
                                </div>
                                <figcaption>
                                    <h3>WBS</h3>
                                    <p>Whistle Blowing System</p>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="<?=$this->request->base;?>/conflict" class="widget-icon-item">
                            <figure>
                                <div class="image">
                                <?php $image = $this->Url->build('/assets/img/wbk/conflict.png');?>
                                    <img src="<?=$image;?>">
                                </div>
                                <figcaption>
                                    <h3>Benturan Kepentingan</h3>
                                    <p>Laporan benturan kepentingan para pejabat/ jajaran BBLK SURABAYA</p>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                </div>
            </section>
        </section>
    </section>
</section>

<?php $this->start('script');?>
    <script>
    </script>
<?php $this->end();?>