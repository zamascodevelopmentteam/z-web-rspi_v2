<?php
    $this->assign('title', 'Jadwal Dokter');
    $this->start('breadcrumb');
        echo 'Jadwal Dokter';
    $this->end();
?>
<section class="container-page" style="">
  <div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12">
          <section class="header-container-page">
            <h1>Tarif Layanan</h1>
          </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Jenis Pelayanan</th>
                    <th>Satuan</th>
                    <th colspan="3">
                        Tarif
                    </th>
                </tr>
            </thead>
            <?=$this->Utilities->generateTarif($rates); ?>
          </table>
          </div>
        </div>
    </div>
  </div>
</section>