<?php
    $this->assign('title', 'Jadwal Dokter');
    $this->start('breadcrumb');
        echo 'Jadwal Dokter';
    $this->end();
?>
<section class="container-page" style="">
  <div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12">
          <section class="header-container-page">
            <h1>Jadwal Dokter</h1>
          </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="table-responsive">
          <?php foreach($poli as $p): ?>
            <table class="table">
              <thead>
                <tr style="background-color: #ddd; text-align: center;">
                  <th colspan="8">Poliklinik : <?= $p->polyclinic ?></th>
                </tr>
                <tr style="text-align: center;">
                  <th>Nama Dokter</th>
                  <th style="background-color: #eee;width:10%;">Senin</th>
                  <th style="width:10%;">Selasa</th>
                  <th style="background-color: #eee;width:10%;">Rabu</th>
                  <th style="width:10%;">Kamis</th>
                  <th style="background-color: #eee;width:10%;" style="width:10%;">Jumat</th>
                  <th style="width:10%;">Sabtu</th>
                  <th style="background-color: #eee;width:10%;">Minggu</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($jadwal as $val): ?>
                <?php if($val->doctors_division->polyclinic_id == $p->id): ?>
                  <tr style="text-align:center">
                    <td><?= $val->doctors_division->doctor->name ?></td>
                    <?php if($val->monday != ""){?>
                      <td style="background-color: #eee;" class="text-success"><i style="font-size: 27px" class="fa fa-check-circle"></i></td>
                    <?php }else{?>
                      <td style="background-color: #eee;" class="text-danger"><i style="font-size: 27px" class="fa  fa-times-circle"></i></td>
                    <?php } ?>
                    

                    <?php if($val->tuesday != ""){?>
                      <td class="text-success"><i style="font-size: 27px" class="fa fa-check-circle"></i></td>
                    <?php }else{?>
                      <td class="text-danger"><i style="font-size: 27px" class="fa  fa-times-circle"></i></td>
                    <?php } ?>

                    <?php if($val->wednesday != ""){?>
                      <td style="background-color: #eee;" class="text-success"><i style="font-size: 27px" class="fa fa-check-circle"></i></td>
                    <?php }else{?>
                      <td style="background-color: #eee;" class="text-danger"><i style="font-size: 27px" class="fa  fa-times-circle"></i></td>
                    <?php } ?>

                    <?php if($val->thursday != ""){?>
                      <td class="text-success"><i style="font-size: 27px" class="fa fa-check-circle"></i></td>
                    <?php }else{?>
                      <td class="text-danger"><i style="font-size: 27px" class="fa  fa-times-circle"></i></td>
                    <?php } ?>

                    <?php if($val->friday != ""){?>
                      <td style="background-color: #eee;" class="text-success"><i style="font-size: 27px" class="fa fa-check-circle"></i></td>
                    <?php }else{?>
                      <td style="background-color: #eee;" class="text-danger"><i style="font-size: 27px" class="fa  fa-times-circle"></i></td>
                    <?php } ?>

                    <?php if($val->saturday != ""){?>
                      <td class="text-success"><i style="font-size: 27px" class="fa fa-check-circle"></i></td>
                    <?php }else{?>
                      <td class="text-danger"><i style="font-size: 27px" class="fa  fa-times-circle"></i></td>
                    <?php } ?>

                    <?php if($val->sunday != ""){?>
                      <td style="background-color: #eee;" class="text-success"><i style="font-size: 27px" class="fa fa-check-circle"></i></td>
                    <?php }else{?>
                      <td style="background-color: #eee;" class="text-danger"><i style="font-size: 27px" class="fa  fa-times-circle"></i></td>
                    <?php } ?>
                  </tr>
                <?php endif; ?>
              <?php endforeach; ?>
              </tbody>
            </table>
            <br>
          <?php endforeach; ?>
          </div>
        </div>
    </div>
  </div>
</section>