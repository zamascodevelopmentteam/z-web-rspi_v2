<?php
    if(!empty($content)):
      $this->assign('title', $content->title);
      $this->assign('meta_description', $content->meta_description);
      $this->assign('meta_keyword', $content->meta_keyword);
      $this->start('breadcrumb');
          echo $content->title;
      $this->end();
?>

<section class="container-page">
  <div class="container">
  	<div class="row">
      <div class="col-lg-8 col-md-12">
          <?php if(!empty($content->picture)):?>
          <div class="image-container-page">
          <img src="<?=$this->Utilities->generateUrlImage($content->picture_dir,$content->picture);?>" class="" alt="<?=$content->title;?>" style="">
          </div>
          <?php endif;?>
          <section class="header-container-page">
            <h1><?=$content->title;?></h1>
          </section>
          <?=$content->body;?>
      </div>
    </div>
  </div>
</section>

<?php

    else:
    $this->assign('title', "HALAMAN TIDAK DITEMUKAN");
?>

      <section class="container-page">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <section class="not-found-page">
                <h1>HALAMAN TIDAK DITEMUKAN</h1>
                <a href="<?=$this->request->base;?>/">Kembali ke menu utama</a>
              </section>
            </div>
          </div>
        </div>
      </section>
<?php

    endif;

?>
