<?php if($content->cover != ""):?>
<div>
    <header style="background-color:#ffffff;background-image:url('<?=$this->Utilities->generateUrlImage($content->cover_dir,$content->cover);?>' );background-repeat:no-repeat;background-size:cover;min-height:400px">
    </header>
</div>
<?php endif;?>
<?php
    $this->assign('title', $content->title);
    $this->assign('meta_keyword', $content->meta_keyword);
    $this->assign('meta_description', $content->meta_description);
?>
<div class="c-berita">
<div class="sec-header vertical-align-bottom">
    <div class="container ">
      <div class="row">
        <div class="col-12"><h1><?=$content->title;?></h1></div>
      </div>
    </div>
  </div>
  <div class="container mt-5">
    <div class="row">
      <div class="col-12">
      Posted by : <?=$content->created_user->name;?> (<?=$content->created->format('d-m-Y');?>)
      </div>
      <div class="col-3">
        <div class="image">
          <img src="<?=$this->Utilities->generateUrlImage($content->picture_dir,'thumbnail-'.$content->picture);?>" style="max-width:100%;">
        </div>
      </div>
      <div class="col-9">
        <!--  -->
        <?=$content->body;?>
      </div>
    </div>
  </div>
  <div class="foot-news">
    <div class="container">
      <div class="row">
        <div class="col-12 mt-4">
          <h3>Related Stories</h3>
          <hr>
        </div>
      </div>
      <?php foreach($content->related_contents as $key => $related):?>
      <div class="row">
        <div class="col-12 p-3 news-list">
          <div class="title">
            <a href="<?=$this->Url->build(['_name'=>'berita_detail','slug'=>$related->slug]);?>"><?=$related->title;?></a>
          </div>
          <div class="col-2 tgl" align="center">
            <?=$related->created->format('d, M Y');?>
          </div>
          <div class="content">
          <?=$this->Text->truncate(
                  strip_tags($related->body),
                  300,
                  [
                      'ellipsis' => '...',
                      'exact' => false
                  ]
              );?>
          </div>
          <div class="foot">
            Posted by: <?=$content->created_user->name;?>
          </div>
        </div>
      </div>
      <?php endforeach;?>
    </div>
  </div>
  </div>