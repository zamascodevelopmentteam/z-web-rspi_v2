<?php
    $this->assign('title', $album->title);
    $this->start('breadcrumb');
        echo $album->title;
    $this->end();
?>

<section class="container-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <section class="header-container-page">
                <h1><?=$album->title;?></h1>
            </section>
            </div>
        </div>
        <div class="row">
            <?php foreach($album->child_albums as $key => $r):?>
            <div class="col-md-4">
                <a href="<?=$this->Url->build(['_name'=>'album_detail','slug'=>$r->slug]);?>" class="album-item">
                    <div class="img-fluid">
                        <i class="fa-folder fa" style="font-size: 40px"></i>
                    </div>
                    <h5><?=$r->title;?></h5>
                </a>
            </div>
            <?php endforeach; ?>
            <?php foreach($album->albums_files as $key => $r):?>
                <?php
                    if($r->file_type == "application/pdf"){
                        $docType = $this->request->base."/assets/webadmin/img/pdf-icon.png";
                        $type = "doc";
                    }else{
                        $docType = "";
                        $type = "image";
                    }
                ?>
            <div class="col-md-4">
                <a href="<?=$this->Utilities->generateUrlImage($r->file_dir,$r->file);?>"  class="album-item" data-type="<?=$type;?>" data-caption="<?=$r->file;?>" data-doc-url="<?=$docType;?>" target="_BLANK">
                    <?php if($r->file_type == "application/pdf"):?>
                        <img src="<?=$this->request->base;?>/assets/img/pdf-icon.png" class="pdf-icon">
                        <?=$r->file;?>
                    <?php else:?>
                        <img src="<?=$this->Utilities->generateUrlImage($r->file_dir,$r->file,'thumbnail-');?>" class="">
                    <?php endif;?>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>