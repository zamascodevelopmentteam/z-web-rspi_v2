<?php
    $this->assign('title', 'Whistle Blowing System / WBS');
    $this->start('breadcrumb');
        echo '<li class="breadcrumb-item"><a href="'.$this->request->base.'/"><i class="fa fa-home"></i> Beranda</a></li>';
        echo '<li class="breadcrumb-item active">Pelaporan Benturan Kepentingan</li>';
    $this->end();
?>
<section class="container wrapper">
    <section class="row">
        <section class="col-md-12">
            <header class="title-wrapper text-left">
                <div class="main-title">
                    <h1>Pelaporan Benturan Kepentingan</h1>
                    <p>Anda melihat benturang kepentingan di lingkungan BBLK Surabaya?. Laporkan ke Satuan Pengawas Internal (SPI) BBLK Surabaya melalui Aplikasi Pelaporan Benturan Kepentingan</p>
                </div>
            </header>
        </section>
    </section>
    <section class="row">
        <section class="col-md-12">
            <section class="body-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <?=$this->Flash->render();?>
                        <?=$this->Form->create($conflictInterest,['class'=>'form-ajax','type'=>'file']);?>
                            <div class="row">
                                <div class="col-md-6">
                                    <?=$this->Form->control('email',['label'=>'Email']);?>
                                    <?=$this->Form->control('name',['label'=>'Nama']);?>
                                    <?=$this->Form->control('phone',['label'=>'No. Handphone']);?>
                                    <?=$this->Form->control('address',['label'=>'Alamat']);?>
                                </div>
                                <div class="col-md-6">
                                    <?=$this->Form->control('message',['label'=>'Isi Laporan','rows'=>10]);?>
                                    <div class="form-group m-form__group">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> KIRIM</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            </div>
                            

                        <?=$this->Form->end();?>
                    </div>
                </div>
            </section>
        </section>
    </section>
</section>

<?php $this->start('script');?>
    <script>
    </script>
<?php $this->end();?>