<?php
    $this->assign('title', 'Whistle Blowing System');
    $this->start('breadcrumb');
        echo 'Whistle Blowing System';
    $this->end();
?>
<section class="container-page" style="">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="contact-form">
                    <section class="header-container-page">
                        <h1>Whistle BLowing System</h1>
                        <p style="">Whistle Blower adalah seseorang yang melaporkan perbuatan dugaaan tindak pidana korupsi yang terjadi di dalam organisasi tempatnya bekerja, atau pihak terkait lainnya yang memiliki akses informasi yang memadai atas terjadinya dugaan tindak pidana korupsi tersebut.</p>
                    </section>
                    <?=$this->Flash->render();?>
                    <?=$this->Form->create($publicComplaint,['class'=>'form-ajax', 'id' => 'ajax-contact']);?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select name="reporter_type" id="reporter-type" placeholder="Jenis Pegawai" class="form-control">
                                    <option value="PEGAWAI">PEGAWAI</option>
                                    <option value="NON PEGAWAI">NON PEGAWAI</option>
                                </select>
                            </div>
                         </div>
                        <div class="col-md-6">
                        <div class="form-group"><input type="text" id="nip" name="nip" placeholder="NIP" class="form-control"></div>
                        </div>  
                        <div class="col-md-6">
                        <div class="form-group"><input type="text" id="name" name="name" placeholder="Nama" class="form-control"></div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group"><input type="text" id="job" name="job" placeholder="Pekerjaan" class="form-control"></div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group"><input type="text" id="origin" name="origin" placeholder="Sumber Pelapor" class="form-control"></div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group"><input type="text" id="province" name="province" placeholder="Provinsi" class="form-control"></div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group"><input type="text" id="city" name="city" placeholder="Kota" class="form-control"></div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group"><input type="text" id="position" name="position" placeholder="Posisi Pekerjaan" class="form-control"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        <div class="form-group"><input type="file" style="" name="file" id="file" class="form-control"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"><textarea name="address" id="address" placeholder="Alamat" required="" class="form-control"></textarea></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"><textarea name="message" id="message" placeholder="Pesan" required="" class="form-control"></textarea></div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Kirim Pesan</button>
                    <?=$this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->start('script');?>
    <script>
        function changeRt(){
            var thisVal = $("#reporter-type").val();
            if(thisVal == 'NON PEGAWAI'){

                <?php if(empty($this->request->data['position'])):?>
                    $("#position").val("");
                    $("#position").removeAttr("required");
                <?php endif;?>
                $("#position").closest(".col-md-6").hide();

                <?php if(empty($this->request->data['nip'])):?>
                    $("#nip").val("");
                    $("#nip").removeAttr("required");
                <?php endif;?>
                $("#nip").closest(".col-md-6").hide();

                $("#job").closest(".col-md-6").show();
                $("#origin").closest(".col-md-6").show();
                $("#province").closest(".col-md-6").show();
                $("#city").closest(".col-md-6").show();
            }else if(thisVal == 'PEGAWAI'){

                <?php if(empty($this->request->data['job'])):?>
                    $("#job").val("");
                    $("#job").removeAttr("required");
                <?php endif;?>
                $("#job").closest(".col-md-6").hide();

                <?php if(empty($this->request->data['origin'])):?>
                    $("#origin").val("");
                    $("#origin").removeAttr("required");
                <?php endif;?>
                $("#origin").closest(".col-md-6").hide();

                <?php if(empty($this->request->data['province'])):?>
                    $("#province").val("");
                    $("#province").removeAttr("required");
                <?php endif;?>
                $("#province").closest(".col-md-6").hide();

                <?php if(empty($this->request->data['city'])):?>
                    $("#city").val("");
                    $("#city").removeAttr("required");
                <?php endif;?>
                $("#city").closest(".col-md-6").hide();

                $("#nip").attr("required",true);
                $("#name").attr("required",true);
                $("#address").attr("required",true);
                $("#position").attr("required",true);

                $("#position").closest(".col-md-6").show();
                $("#nip").closest(".col-md-6").show();
            }
        }
        changeRt();
        $("#reporter-type").on("change",function(e){
            changeRt()
        })
    </script>
<?php $this->end();?>
