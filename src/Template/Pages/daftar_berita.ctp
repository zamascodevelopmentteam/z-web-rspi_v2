<?php
    $this->assign('title', $category);
    $this->start('breadcrumb');
        echo $category;
    $this->end();
?>
<section class="container-page">
	<div class="container">
		<?php if($artikels->count() == 0):?>
            <?php if($this->request->category == "berita"):?>
                <h3>Berita tidak ditemukan</h3>
            <?php else:?>
                <h3>Artikel Kesehatan tidak ditemukan</h3>
            <?php endif;?>
		<?php else:?>
		<section class="row">
			<section class="col-lg-4 col-md-6">
				<section class="header-container-page">
					<h1>Berita & Kegiatan Terbaru</h1>
				</section>
			</section>
		</section>
		<div class="row">
			<?php foreach($artikels as $key => $artikel):?>
                <?php 
                    $image = $this->Utilities->generateUrlImage($artikel->picture_dir,$artikel->picture,'thumbnail-');
                    $url = $this->Url->build([
                        'controller'=>'Pages',
                        'action'=>'display',
                        'category'=>$artikel->contents_category->slug,
                        'slug'=>$artikel->slug
                    ]);
            	?>
				<div class="col-lg-3 col-md-3">
					<a class="blog-box" href="<?= $url; ?>">
						<div class="blog-image">
							<img src="<?=$image;?>" alt="" class="">
						</div>
						<div class="blog-heading">
							<div class="heading-box">
								<h4><?=$this->Text->truncate($artikel->title,100,['ellipsis'=>'...']);?></h4>
							</div>
						</div>
						<div class="blog-content">
							<p><?=$this->Text->truncate(strip_tags($artikel->body),150,['ellipsis'=>'...']);?></p>
						</div>
					</a>
				</div>
				<?php endforeach; ?>
			</div>
			<?php endif;?>
		</div>
	</div>
</section>
