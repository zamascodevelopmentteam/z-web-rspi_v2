<section class="container-page">
	<section class="container">
		<section class="row">
			<section class="col-lg-4">
				<div class="head-of-institute">
					<img src="<?=$this->request->base;?>/assets/img/rita-dr.png">
					<div class="caption">
						<h3>dr. Rita Rogayah, Sp.P(K), MARS</h3>
						<p>Direktur Utama RSPI Sulianti Saroso</p>
					</div>
				</div>
			</section>
			<section class="col-lg-8">
				<div class="welcome-message">
					<h1>Selamat Datang</h1>
					<p>
					Puji dan syukur marilah kita panjatkan kehadirat Tuhan YME, yang atas perkenan-NYA maka website RSPI Sulianti Saroso ini dapat beroperasional (online)
					<br>
					<br>
					Website ini memuat informasi mengenai Profil RSPI Sulianti Saroso, Pelayanan Kesehatan yang ada serta berita seputar kegiatan RSPI Sulianti Saroso dalam melaksanakan kegiatan pelayanan kesehatan
					<br>
					<br>
					Website ini diharapkan dapat memberikan informasi yang lebih utuh kepada masyarakat untuk mendapatkan Pelayanan Kesehatan yang sesuai dengan kebutuhannya
					<br>
					<br>
					Kami menyadari, bahwa website ini masih jauh dari sempurna, sehingga kritik dan saran yang membangun tentunya sangat kami harapkan dari masyarakat sehingga RSPI dapat memberikan pelayanan kesehatan yang lebih baik lagi
					</p>
				</div>
			</section>
		</section>
	</section>
</section>
<section class="container-page">
	<section class="container">
		<section class="row">
			<?php foreach($links['SHORTCUT PRIMARY'] as $key => $layanan):?>
				<?php 
					$image = $this->Utilities->generateUrlImage($layanan['picture_dir'],$layanan['picture'],'');
					$url = $this->Utilities->generateUrlMenu($layanan);
				?>
				<div class="col-md-3">
					<a class="shortcut-home-icon" href="<?=$url;?>" style="">
						<div class="image-icon">
							<img src="<?= $image; ?>" alt="">
						</div>
						<p><?=$layanan['title'];?></p>
					</a>
				</div>
			<?php endforeach;?>
		</section>
	</section>
</section>
<section class="container-page">
	<section class="container">
		<section class="row">
			<section class="col-lg-4 col-md-6">
				<section class="header-container-page">
					<h1>Berita & Kegiatan Terbaru</h1>
				</section>
			</section>
		</section>
		<section class="row">
			<?php foreach($artikels as $key => $artikel):?>
			<?php 
				$image = $this->Utilities->generateUrlImage($artikel->picture_dir,$artikel->picture,'thumbnail-');
				$url = $this->Url->build([
					'controller'=>'Pages',
					'action'=>'display',
					'category'=>$artikel->contents_category->slug,
					'slug'=>$artikel->slug
				]);
			?>
			<div class="col-lg-3 col-md-3">
				<a class="blog-box" href="<?= $url; ?>">
					<div class="blog-image">
						<img src="<?=$image;?>" alt="" class="">
					</div>
					<div class="blog-heading">
						<div class="heading-box">
							<h4><?=$this->Text->truncate($artikel->title,100,['ellipsis'=>'...']);?></h4>
						</div>
					</div>
					<div class="blog-content">
						<p><?=$this->Text->truncate(strip_tags($artikel->body),150,['ellipsis'=>'...']);?></p>
					</div>
				</a>
			</div>
			<?php endforeach; ?>
		</section>
	</section>
</section>
<section class="container-page bg-video">
	<section class="bg-video-default">
		<img src="<?=$this->request->base;?>/assets/img/doctors.jpg" class="bg-image">
		<section class="container">
			<div class="video-caption">
				<h3>Tentang Kami</h3>
				<p>Kenali kami lebih lanjut</p>
				<a href="#" class="play-icon">
					<i class="fa fa-play"></i> <span>Putar Video</span>
				</a>
			</div>
		</section>
	</section>
	<section class="video-container">
		<section class="video-control">
			<a href="#" class="video-pause">
				<i class="fa fa-pause"></i>
			</a>
			<a href="#" class="video-stop">
				<i class="fa fa-stop"></i>
			</a>
			<a href="#" class="video-close">
				<i class="fa fa-times"></i>
			</a>
		</section>
		<video id="video-compro">
			<source src="<?=$this->request->base;?>/assets/video/rspi-profile.mp4" type="video/mp4">
			Your browser does not support the video tag.
		</video>
	</section>
</section>