<?php
  $this->assign('title', 'Hubungi Kami');
  $this->start('breadcrumb');
      echo '<li class="breadcrumb-item"><a href="'.$this->request->base.'/"><i class="fa fa-home"></i> Beranda</a></li>';
      echo '<li class="breadcrumb-item active">Hubungi Kami</li>';
  $this->end();
?>
<section class="container-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-5">
                <section class="header-container-page">
            		<h1>Info Kontak</h1>
            	</section>
                <div class="row">
                    <div class="col-md-12">
                        <div class="contact-box">
                            <div class="contact-icon">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="contact-content">
                                <h6>Lokasi</h6>
                                <p><?=$defaultAppSettings['Web.Location.Address'];?></p>
                            </div>
                        </div>
                        <div class="contact-box">
                            <div class="contact-icon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="contact-content">
                                <h6>Email</h6>
                                <p><?=$defaultAppSettings['Web.Location.Email'];?></p>
                            </div>
                        </div>
                        <div class="contact-box ">
                            <div class="contact-icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="contact-content">
                                <h6>No. Telpon</h6>
                                <p><?=$defaultAppSettings['Web.Location.Phone.Number'];?></p>
                                <p>FAX <?=$defaultAppSettings['Web.Location.Fax.Number'];?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-7">
                <div class="contact-form">
                    <section class="header-container-page">
                        <h1>Hubungi Kami</h1>
                    </section>
                    <?=$this->Flash->render();?>
                    <?=$this->Form->create($contactUs,['class'=>'form-ajax', 'id' => 'ajax-contact']);?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" id="name" name="name" placeholder="Full Name" required="" class="form-control">
                            </div>
                         </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" id="email" name="email" placeholder="Email" required="" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                             <textarea name="message" id="message" placeholder="Message" required="" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Kirim Pesan</button>
                        </div>
                    </div>
                    <?=$this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->start('script');?>
    <script>
      var map;
      function initMap() {
        var uluru = {lat: <?= $defaultAppSettings['Web.Location.Longitude']?>, lng: <?= $defaultAppSettings['Web.Location.Latitude']?>};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAinsXv01FkdYo3hk8JAJTRZqOsGlnCzZQ&callback=initMap">
    </script>
<?php $this->end();?>
