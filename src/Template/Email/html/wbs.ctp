<table>
    <tr>
        <td colspan="3">Telah dilakukan pengadauan WBS pada tanggal : <?=date('Y-m-d');?></td>    
    </tr>
    <tr>
        <td width="15%">Jenis Pelapor</td>
        <td width="5%">:</td>
        <td><?=$data->reporter_type;?></td>
    </tr>
    <?php if($data->reporter_type == "PEGAWAI"):?>
        <tr>
            <td>Unit Kerja</td>
            <td>:</td>
            <td><?=$data->work_unit;?></td>
        </tr>
        <tr>
            <td>Jabatan</td>
            <td>:</td>
            <td><?=$data->position;?></td>
        </tr>
        <tr>
            <td>Surat Tugas Lapor</td>
            <td>:</td>
            <td><?=$data->letter_of_assignment;?></td>
        </tr>
    <?php endif;?>
    <tr>
        <td>Isi Pengaduan</td>
        <td>:</td>
        <td><?=$data->message;?></td>
    </tr>
</table>