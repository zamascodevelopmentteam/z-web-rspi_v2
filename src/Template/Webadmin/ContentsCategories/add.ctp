
<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($contentsCategory,['class'=>'m-form m-form--fit m-form--label-align-right']) ?>
    <div class="m-portlet__body">
        
        <div class="row m--margin-bottom-15">
            <div class="col-md-6">
                <?= $this->Form->control('title');?>
            </div>
        
            <div class="col-md-6">
                <?= $this->Form->control('slug');?>
            </div>
        </div>
        <div class="row m--margin-bottom-15">
            <div class="col-md-6">
                <?= $this->Form->control('meta_description');?>
            </div>
        
            <div class="col-md-6">
                <?= $this->Form->control('meta_keyword');?>
            </div>
        </div>
        <div class="row m--margin-bottom-15">
            <div class="col-md-8 offset-md-2">
                <?=$this->Form->control('status',['options' => ['DISABLED','ENABLED']],[
                    'label' => 'Status',
                ]);?>
            </div>
        </div>
    </div>  
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <?= $this->Form->button(__('Submit'),['class'=>'btn btn-primary']) ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>