<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <table class="table">
        <tr>
            <th scope="row"><?= __('Tipe Pelapor') ?></th>
            <td><?= $publicComplaint->reporter_type ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nama') ?></th>
            <td><?= $publicComplaint->name ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Alamat') ?></th>
            <td><?= $publicComplaint->address ?></td>
        </tr>
        <?php if($publicComplaint->reporter_type == "NON PEGAWAI"): ?>
            <tr>
                <th scope="row"><?= __('Pekerjaan') ?></th>
                <td><?= $publicComplaint->job ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Provinsi') ?></th>
                <td><?= $publicComplaint->province ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Kota') ?></th>
                <td><?= $publicComplaint->city ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Sumber Pelapor') ?></th>
                <td><?= $publicComplaint->origin ?></td>
            </tr> 
        <?php else: ?>
        <tr>
            <th scope="row"><?= __('Position') ?></th>
            <td><?= $publicComplaint->position ?></td>
        </tr> 
        <?php endif; ?>
        <tr>
            <th scope="row"><?= __('NIP') ?></th>
            <td><?= $publicComplaint->nip ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pengaduan') ?></th>
            <td><?= $publicComplaint->message ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Barang Bukti') ?></th>
            <td><img src="<?= $this->Utilities->generateUrlImage($publicComplaint->file_dir,"thumbnail-".$publicComplaint->file) ?>" width="150px" style="max-width:100%" ></td>
        </tr>   
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($publicComplaint->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($publicComplaint->created) ?></td>
        </tr>
        </table>
        <!--end: Datatable -->
    </div>
</div>
