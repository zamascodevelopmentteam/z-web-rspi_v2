<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($doctorSchedule,['class'=>'m-form m-form--fit m-form--label-align-right']) ?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-12">
                    <!-- <?=$this->Form->control('doctor_id', ['options'=>$doctors, 'label' => 'Nama Dokter']);?> -->
                    <select name="doctors_division_id" id="doctors-division-id" class="form-control">
                        <?php foreach($doctors as $val){ ?>
                        <option value="<?= $val->id?>"><?= $val->name?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('monday', ['type' => 'text', 'label' => 'Senin']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('tuesday',['type'=>'text','label'=>'Selasa']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('wednesday', ['type' => 'text', 'label' => 'Rabu']);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('thursday',['type'=>'text','label'=>'Kamis']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('friday', ['type' => 'text', 'label' => 'Jumat']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('saturday', ['type' => 'text', 'label' => 'Sabtu']);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('sunday',['type'=>'text','label'=>'Minggu']);?>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        $('#doctors-division-id').select2();
        $('#time-start').timepicker();
        $('#time-end').timepicker();
    </script>
<?php $this->end();?>
