<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($dataSave,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->control('Web_Name',[
                        'label'=>'Web Name',
                        'value' => $webSettings['Web_Name']->valueField
                    ]);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('Web_Copyright',[
                        'label'=>'Web Copyright',
                        'value' => $webSettings['Web_Copyright']->valueField
                    ]);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-3">
                    <?=$this->Form->control('Web_Logo_Width',[
                        'label'=>'Web Logo Width',
                        'value' => $webSettings['Web_Logo_Width']->valueField
                    ]);;?>
                </div>
                <div class="col-md-3">
                    <?=$this->Form->control('Web_Logo_Height',[
                        'label'=>'Web Logo Height',
                        'value' => $webSettings['Web_Logo_Height']->valueField
                    ]);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('Web_Logo',[
                        'label'=>'Web Logo',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->control('Web_Description',[
                        'label'=>'Web Short Description',
                        'value' => $webSettings['Web_Description']->valueField,
                        'type' => 'textarea'
                    ]);;?>
                </div>
                <div class="col-md-6">
                    <?=$this->Form->control('Web_Location_Address',[
                        'label'=>'Web Location Address',
                        'value' => $webSettings['Web_Location_Address']->valueField,
                        'type' => 'textarea',
                        'rows' => '3'
                    ]);;?>
                </div>
            
            </div>
            
            <div class="row m--margin-bottom-15">
                <div class="col-md-3">
                    <?=$this->Form->control('Web_Location_Phone_Name',[
                        'label'=>'Web Phone Name',
                        'class' => 'form-control m-input',
                        'value' => $webSettings['Web_Location_Phone_Name']->valueField,
                    ]);;?>
                </div>
                <div class="col-md-3">
                    <?=$this->Form->control('Web_Location_Phone_Number',[
                        'label'=>'Web Phone Number',
                        'class' => 'form-control m-input',
                        'value' => $webSettings['Web_Location_Phone_Number']->valueField,
                    ]);;?>
                </div>
                <div class="col-md-3">
                    <?=$this->Form->control('Web_Location_Fax_Number',[
                        'label'=>'Web Fax Number',
                        'class' => 'form-control m-input',
                        'value' => $webSettings['Web_Location_Fax_Number']->valueField,
                    ]);;?>
                </div>
                <div class="col-md-3">
                    <?=$this->Form->control('Web_Location_Email',[
                        'label'=>'Web Email',
                        'class' => 'form-control m-input',
                        'value' => $webSettings['Web_Location_Email']->valueField,
                    ]);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-3">
                    <?=$this->Form->control('Web_Location_Longitude',[
                        'label'=>'Web Location Logitude',
                        'class' => 'form-control m-input',
                        'value' => $webSettings['Web_Location_Longitude']->valueField,
                    ]);;?>
                </div>
                <div class="col-md-3">
                    <?=$this->Form->control('Web_Location_Latitude',[
                        'label'=>'Web Location Latitude',
                        'class' => 'form-control m-input',
                        'value' => $webSettings['Web_Location_Latitude']->valueField,
                    ]);;?>
                </div>
                <div class="col-md-3">
                    <?=$this->Form->control('Web_Location_Link',[
                        'label'=>'Web Location Link',
                        'class' => 'form-control m-input',
                        'value' => $webSettings['Web_Location_Link']->valueField,
                    ]);;?>
                </div>
            </div>

            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('Web_Favico',[
                        'label'=>'Web Favico',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('Web_FirstPage',[
                        'label'=>'Web First Page',
                        'class' => 'form-control m-input',
                        'value' => $webSettings['Web_FirstPage']->valueField,
                    ]);;?>
                </div>
            </div>
            <h5>Sosial Media</h5>
            <hr>
            <div class="row m--margin-bottom-15">
                <div class="col-md-3">
                    <?=$this->Form->control('Web_Instagram',[
                        'label'=>'Web Instagram',
                        'class' => 'form-control m-input',
                        'value' => $webSettings['Web_Instagram']->valueField,
                    ]);;?>
                </div>
                <div class="col-md-3">
                    <?=$this->Form->control('Web_Facebook',[
                        'label'=>'Web Facebook',
                        'class' => 'form-control m-input',
                        'value' => $webSettings['Web_Facebook']->valueField,
                    ]);;?>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php
    $this->Html->script([
        'dist/vendors/custom/ckeditor/ckeditor.js'
    ],['block'=>'jsMain','pathPrefix' => '/assets/webadmin/']);
?>
<?php $this->start('script');?>
    <script>
        $("#inspection-packages-details").select2();
        var SummernoteDemo = function () {    
        //== Private functions
        var demos = function () {
            CKEDITOR.replace( 'web-description', {
                on: {
                    instanceReady: function() {
                        console.log(this.document);
                        this.document.appendStyleSheet('<?=$this->request->base;?>' + '/assets/dist/css/style.bundle.css');
                    }
                },
                extraPlugins: 'easyimage',
                removePlugins: 'image',
                cloudServices_tokenUrl: '<?=$this->Url->build(['controller'=>'Pages','action' => 'getTokenMedia']);?>',
                cloudServices_uploadUrl: '<?=$this->Url->build(['controller'=>'Pages','action' => 'uploadMedia']);?>',
                easyimage_toolbar : [
                    'EasyImageAlignLeft',
                    'EasyImageAlignCenter',
                    'EasyImageAlignRight'
                ]
            });
        }
    
        return {
            // public functions
            init: function() {
                demos(); 
            }
        };
    }();
    
    
    jQuery(document).ready(function() {   
        SummernoteDemo.init();
    });
    </script>
<?php $this->end();?>