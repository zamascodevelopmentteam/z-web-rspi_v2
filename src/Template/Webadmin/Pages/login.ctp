<div class="m-stack__item m-stack__item--fluid">
    <div class="m-login__wrapper">
        <div class="m-login__logo">
            <a href="#">
                <img src="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Logo.Login']);?>">
            </a>
        </div>
        <div class="m-login__signin">
            <div class="m-login__head">
                <h3 class="m-login__title">
                    Sign In To <?=$defaultAppSettings['App.Name'];?> Panel
                </h3>
            </div>
            <?=$this->Form->create(null,['class'=>'m-login__form m-form']);?>
                <?=$this->Flash->render();?>
                <?=$this->Form->controls([
                    'username' => ['label'=>false,'autocomplete'=>'off','placeholder'=>'Username'],
                ],[
                    'legend'=>false,
                    'fieldset' => false
                ]);?>
                <?php
                    $this->Form->setTemplates([
                        'input' => '<input type="{{type}}" name="{{name}}" {{attrs}}/>',
                    ]);
                ?>
                <?=$this->Form->controls([
                    'password' => ['label'=>false,'autocomplete'=>'off','placeholder'=>'Password','class'=>'form-control m-input m-login__form-input--last',],
                ],[
                    'legend'=>false,
                    'fieldset' => false
                ]);?>
                <!-- <div class="row m-login__form-sub">
                    <div class="col m--align-left">
                        <label class="m-checkbox m-checkbox--focus">
                            <input type="checkbox" name="remember">
                            Remember me
                            <span></span>
                        </label> 
                    </div>
                    <div class="col m--align-right">
                        <a href="javascript:;" id="m_login_forget_password" class="m-link">
                            Forget Password ?
                        </a> 
                    </div>
                </div> -->
                <div class="m-login__form-action">
                    <button id="m_login_signin_submit" type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                        Sign In
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>