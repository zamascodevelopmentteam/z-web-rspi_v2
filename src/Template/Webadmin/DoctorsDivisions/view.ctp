<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DoctorsDivision $doctorsDivision
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Doctors Division'), ['action' => 'edit', $doctorsDivision->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Doctors Division'), ['action' => 'delete', $doctorsDivision->id], ['confirm' => __('Are you sure you want to delete # {0}?', $doctorsDivision->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Doctors Divisions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Doctors Division'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Doctors'), ['controller' => 'Doctors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Doctor'), ['controller' => 'Doctors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Polyclinics'), ['controller' => 'Polyclinics', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Polyclinic'), ['controller' => 'Polyclinics', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="doctorsDivisions view large-9 medium-8 columns content">
    <h3><?= h($doctorsDivision->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Doctor') ?></th>
            <td><?= $doctorsDivision->has('doctor') ? $this->Html->link($doctorsDivision->doctor->name, ['controller' => 'Doctors', 'action' => 'view', $doctorsDivision->doctor->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Polyclinic') ?></th>
            <td><?= $doctorsDivision->has('polyclinic') ? $this->Html->link($doctorsDivision->polyclinic->id, ['controller' => 'Polyclinics', 'action' => 'view', $doctorsDivision->polyclinic->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($doctorsDivision->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= $this->Number->format($doctorsDivision->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified By') ?></th>
            <td><?= $this->Number->format($doctorsDivision->modified_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($doctorsDivision->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($doctorsDivision->modified) ?></td>
        </tr>
    </table>
</div>
