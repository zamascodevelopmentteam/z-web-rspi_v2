<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($doctorsDivision,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->control('doctor_id', ['label' => 'Nama Dokter', 'empty' => 'Pilih Dokter', 'readonly']);?>
                </div>
                <div class="col-md-6">
                    <?=$this->Form->control('polyclinic_id', ['options' => $polyclinics, 'label' => 'Poliklinik','empty' => 'Pilih Poliklinik', 'required']);?>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        // $('#doctor-id').select2();
        $('#polyclinic-id').select2();
    </script>
<?php $this->end();?>
