<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cover $cover
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Cover'), ['action' => 'edit', $cover->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cover'), ['action' => 'delete', $cover->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cover->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Covers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cover'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="covers view large-9 medium-8 columns content">
    <h3><?= h($cover->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($cover->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('File') ?></th>
            <td><?= h($cover->file) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('File Dir') ?></th>
            <td><?= h($cover->file_dir) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($cover->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Url') ?></th>
            <td><?= $this->Number->format($cover->url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Content Id') ?></th>
            <td><?= $this->Number->format($cover->content_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sort') ?></th>
            <td><?= $this->Number->format($cover->sort) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created By') ?></th>
            <td><?= $this->Number->format($cover->created_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified By') ?></th>
            <td><?= $this->Number->format($cover->modified_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($cover->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($cover->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $cover->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
