<div class="m-portlet m-portlet--full-height">
    <!--begin: Portlet Head-->
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
    </div>
    <!--end: Portlet Head-->
<!--begin: Portlet Body-->
    <div class="m-portlet__body m-portlet__body--no-padding">
        <!--begin: Form Wizard-->
        <div class="m-wizard m-wizard--4 m-wizard--brand m-wizard--step-first" id="m_wizard">
            <div class="row m-row--no-padding">
                <div class="col-xl-3 col-lg-12 m--padding-top-20 m--padding-bottom-15">
                    <!--begin: Form Wizard Head -->
                    <div class="m-wizard__head">
                        <!--begin: Form Wizard Nav -->
                        <div class="m-wizard__nav">
                            <div class="m-wizard__steps">
                                <div class="m-wizard__step m-wizard__step--current" data-wizard-target="#m_wizard_form_step_1">
                                    <div class="m-wizard__step-info">
                                        <a href="#" class="m-wizard__step-number">
                                            <span>
                                                <span>
                                                    1
                                                </span>
                                            </span>
                                        </a>
                                        <div class="m-wizard__step-label">
                                            Content Informations
                                        </div>
                                        <div class="m-wizard__step-icon">
                                            <i class="la la-check"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_2">
                                    <div class="m-wizard__step-info">
                                        <a href="#" class="m-wizard__step-number">
                                            <span>
                                                <span>
                                                    2
                                                </span>
                                            </span>
                                        </a>
                                        <div class="m-wizard__step-label">
                                            Content Body
                                        </div>
                                        <div class="m-wizard__step-icon">
                                            <i class="la la-check"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end: Form Wizard Nav -->
                    </div>
                    <!--end: Form Wizard Head -->
                </div>
                <div class="col-xl-9 col-lg-12">
                    <!--begin: Form Wizard Form-->
                    <div class="m-wizard__form">
                        <!--
    1) Use m-form--label-align-left class to alight the form input lables to the right
    2) Use m-form--state class to highlight input control borders on form validation
-->
                        <?= $this->Form->create($content,['class'=>'m-form m-form--label-align-left- m-form--state-','id'=>'m_form','novalidate'=>'novalidate']) ?>
                            <!--begin: Form Body -->
                            <div class="m-portlet__body m-portlet__body--no-padding">
                                <!--begin: Form Wizard Step 1-->
                                <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                                    <div class="m-form__section m-form__section--first">
                                        <div class="form-group m-form__group row">
                                            <div class="col-md-6">
                                                <?=$this->Form->control('title',[
                                                    'label' => '* Title'
                                                ]);?>
                                            </div>
                                            <div class="col-md-6">
                                                <?=$this->Form->control('contents_category_id',['options' => $contentsCategories,'empty'=>'Choose one'],[
                                                    'label' => '* Content Category',
                                                ]);?>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-md-6">
                                                <?=$this->Form->control('meta_description',[
                                                    'label' => '* Meta Description'
                                                ]);?>
                                            </div>
                                            <div class="col-md-6">
                                                <?=$this->Form->control('meta_keyword',[
                                                    'label' => '* Meta Keyword',
                                                ]);?>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-md-6">
                                                <?=$this->Form->control('picture',[
                                                    'label' => '* Picture',
                                                    'class' => 'form-control m-input',
                                                    'type' => 'file'
                                                ]);?>
                                            </div>
                                            <div class="col-md-6">
                                                <?=$this->Form->control('status',['options' => ['DISABLED','ENABLED']],[
                                                    'label' => 'Status',
                                                ]);?>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-md-6">
                                                <?=$this->Form->control('sort',[
                                                    'label' => 'Sort',
                                                ]);?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Step 1-->
        <!--begin: Form Wizard Step 2-->
                                <div class="m-wizard__form-step" id="m_wizard_form_step_2">
                                    <div class="m-form__section m-form__section--first">
                                        <div class="form-group m-form__group row">
                                            <div class="col-md-12">
                                                <?=$this->Form->control('body',[
                                                    'label' => '* Body Content',
                                                    'type' => 'textarea',
                                                ]);?>
                                                <!-- <div class="summernote" id="m_summernote_1"></div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Step 2-->
                            </div>
                            <!--end: Form Body -->
    <!--begin: Form Actions -->
                            <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-lg-6 m--align-left">
                                            <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                                <span>
                                                    <i class="la la-arrow-left"></i>
                                                    &nbsp;&nbsp;
                                                    <span>
                                                        Back
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="col-lg-6 m--align-right">
                                            <a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
                                                <span>
                                                    <i class="la la-check"></i>
                                                    &nbsp;&nbsp;
                                                    <span>
                                                        Submit
                                                    </span>
                                                </span>
                                            </a>
                                            <a href="#" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                                <span>
                                                    <span>
                                                        Save &amp; Continue
                                                    </span>
                                                    &nbsp;&nbsp;
                                                    <i class="la la-arrow-right"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Actions -->
                        </form>
                    </div>
                    <!--end: Form Wizard Form-->
                </div>
            </div>
        </div>
        <!--end: Form Wizard-->
    </div>
    <!--end: Portlet Body-->
</div>
<?php
    $this->Html->script([
        'dist/vendors/custom/ckeditor/ckeditor.js'
    ],['block'=>'jsMain','pathPrefix' => '/assets/webadmin/']);
?>
<?php $this->start('script');?>
    <script>
    var WizardDemo = function () {
        //== Base elements
        var wizardEl = $('#m_wizard');
        var formEl = $('#m_form');
        var validator;
        var wizard;
        
        //== Private functions
        var initWizard = function () {
            //== Initialize form wizard
            wizard = wizardEl.mWizard({
                startStep: 1
            });
    
            //== Validation before going to next page
            wizard.on('beforeNext', function(wizard) {
                if (validator.form() !== true) {
                    return false;  // don't go to the next step
                }
            })
    
            //== Change event
            wizard.on('change', function(wizard) {
                mApp.scrollTop();
            });
        }
    
        var initValidation = function() {
            validator = formEl.validate({
                //== Validate only visible fields
                ignore: ":hidden",
    
                //== Validation rules
                rules: {
                    title: {
                        required: true 
                    },
                    contents_category_id: {
                        required: true,
                    },       
                    picture: {
                        extension: "jpg|png|jpeg|JPG|PNG|JPEG"
                    },     
                    cover: {
                        extension: "jpg|png"
                    },
                },
    
                //== Validation messages
                //== Display error  
                invalidHandler: function(event, validator) {     
                    mApp.scrollTop();
    
                    swal({
                        "title": "", 
                        "text": "There are some errors in your submission. Please correct them.", 
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                },
    
                //== Submit valid form
                submitHandler: function (form) {
                    
                }
            });   
        }
    
        var initSubmit = function() {
            var btn = formEl.find('[data-wizard-action="submit"]');
    
            btn.on('click', function(e) {
                e.preventDefault();
                for ( instance in CKEDITOR.instances ){
                    CKEDITOR.instances[instance].updateElement();
                }
                    
                if (validator.form()) {
                    //== See: src\js\framework\base\app.js
                    //mApp.progress(btn);
                    mApp.block(formEl); 
                    var data = new FormData(formEl[0]);
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: formEl.attr("action"),
                        data: data,
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        cache: false,
                        timeout: 600000,
                        success: function (data) {
                            mApp.unblock(formEl);
                            if(data.code == 200){
                                swal({
                                    "title": "", 
                                    "text": data.message, 
                                    "type": "success",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide",
                                    preConfirm: () => {
                                        document.location.href = "<?=$this->Url->build(['action'=>'index']);?>"
                                    }
                                });
                            }else{
                                swal({
                                    "title": "", 
                                    "text": "Something not right, please try again", 
                                    "type": "error",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                            }
                        },
                        error: function (e) {
                            mApp.unblock(formEl);
                            swal({
                                "title": "", 
                                "text": "Something not right, please try again", 
                                "type": "error",
                                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                            });
                        }
                    });
                }
            });
        }
    
        return {
            // public functions
            init: function() {
                wizardEl = $('#m_wizard');
                formEl = $('#m_form');
    
                initWizard(); 
                initValidation();
                initSubmit();
            }
        };
    }();
    var SummernoteDemo = function () {    
        //== Private functions
        var demos = function () {
            CKEDITOR.replace( 'body', {
                on: {
                    instanceReady: function() {
                        console.log(this.document);
                        this.document.appendStyleSheet('<?=$this->request->base;?>' + '/assets/dist/css/style.bundle.css');
                    }
                },
                extraPlugins: 'easyimage',
                removePlugins: 'image',
                cloudServices_tokenUrl: '<?=$this->Url->build(['controller'=>'Pages','action' => 'getTokenMedia']);?>',
                cloudServices_uploadUrl: '<?=$this->Url->build(['controller'=>'Pages','action' => 'uploadMedia']);?>',
                easyimage_toolbar : [
                    'EasyImageAlignLeft',
                    'EasyImageAlignCenter',
                    'EasyImageAlignRight'
                ]
            });
        }
    
        return {
            // public functions
            init: function() {
                demos(); 
            }
        };
    }();
    
    
    jQuery(document).ready(function() {    
        WizardDemo.init();
        SummernoteDemo.init();
    });
    </script>
<?php $this->end();?>