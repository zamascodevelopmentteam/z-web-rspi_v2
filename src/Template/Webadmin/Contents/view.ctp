<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <table class="table">
        <tr>
            <th scope="row" width="20%"><?= __('Title') ?></th>
            <td><?= h($content->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Content Category') ?></th>
            <td><?= h($content->contents_category->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Thumbnail') ?></th>
            <td><img src="<?= $this->Utilities->generateUrlImage($content->picture_dir,"thumbnail-".$content->picture) ?>" width="150px" style="max-width:100%" ></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cover') ?></th>
            <td><img src="<?= $this->Utilities->generateUrlImage($content->cover_dir,"thumbnail-".$content->cover) ?>" width="750px" style="max-width:100%" ></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Meta Description') ?></th>
            <td><?= $content->meta_description?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Meta Keyword') ?></th>
            <td><?= $content->meta_keyword?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Body') ?></th>
            <td><?= $content->body?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($content->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($content->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($content->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Utilities->statusLabel($content->status)?></td>
        </tr>
        </table>
        <!--end: Datatable -->
    </div>
</div>
