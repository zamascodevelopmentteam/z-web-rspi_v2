<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <table class="table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($linksMap->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($linksMap->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($linksMap->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($linksMap->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Utilities->statusLabel($linksMap->status)?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Links') ?></th>
            <td>
                <?php foreach($linksMap->links as $key => $link):?>
                    <li><?=$link->title;?> - <?=$link->url;?></li>
                <?php endforeach;?>
            </td>
        </tr>
        </table>
        <!--end: Datatable -->
    </div>
</div>
