<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">
    <!-- BEGIN: Aside Menu -->
    <div
        id="m_ver_menu"
        class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light "
        data-menu-vertical="true"
        data-menu-scrollable="false" data-menu-dropdown-timeout="500"
    >
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <?php if(array_key_exists('Dashboard',$sidebarList)):?>
                <li class="m-menu__item <?=($this->request->controller=='Dashboard' ? 'm-menu__item--active' : '');?>" aria-haspopup="true" >
                    <a  href="<?=$this->Url->build(['controller'=>'Dashboard','action'=>$sidebarList['Dashboard']]);?>" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-line-graph"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">
                                    Dashboard
                                </span>
                            </span>
                        </span>
                    </a>
                </li>
            <?php endif;?>
            <?php
                $countCheckSideBar = $this->Utilities->sideBarArrayCheck($sidebarList,['Doctors','Polyclinics']);
                if($countCheckSideBar > 0):
            ?>
                <li class="m-menu__section">
                    <h4 class="m-menu__section-text">
                        Master
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                <?php
                    $navList = (object)[
                        'Polyclinics' => (object)[
                            'label' => 'Poliklinik',
                            'icon' => 'fa fa-plus',
                            'url'  => $this->Url->build(['controller'=>'Polyclinics','action'=>'index'])
                        ],
                        'Doctors' => (object)[
                            'label' => 'Dokter',
                            'icon' => 'fa fa-user-md',
                            'url'  => $this->Url->build(['controller'=>'Doctors','action'=>'index'])
                        ],
                        'DoctorsDivisions' => (object)[
                            'label' => 'Divisi Dokter',
                            'icon' => 'fa fa-deviantart',
                            'url'  => $this->Url->build(['controller'=>'DoctorsDivisions','action'=>'index'])
                        ],
                    ];
                    foreach($navList as $key => $nav):
                        if(array_key_exists($key,$sidebarList)):
                ?>
                        <li class="m-menu__item  <?=($this->request->controller==$key ? 'm-menu__item--active' : '');?>" aria-haspopup="true" >
                            <a  href="<?=$nav->url;?>" class="m-menu__link ">
                                <i class="m-menu__link-icon <?=$nav->icon;?>"></i>
                                <span class="m-menu__link-title">
                                    <span class="m-menu__link-wrap">
                                        <span class="m-menu__link-text">
                                            <?=$nav->label;?>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <?php endif;?>
                    <?php endforeach;?>
            <?php endif;?>
            <?php
                $countCheckSideBar = $this->Utilities->sideBarArrayCheck($sidebarList,['ContentsCategories','Contents','LinkMaps','Links']);
                if($countCheckSideBar > 0):
            ?>
                <li class="m-menu__section">
                    <h4 class="m-menu__section-text">
                        Manajemen Konten
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                <?php
                    $navList = (object)[
                        'ContentsCategories' => (object)[
                            'label' => 'Kategori Konten',
                            'icon' => 'flaticon-lifebuoy',
                            'url'  => $this->Url->build(['controller'=>'ContentsCategories','action'=>'index'])
                        ],
                        'Contents' => (object)[
                            'label' => 'Konten',
                            'icon' => 'flaticon-lifebuoy',
                            'url' => $this->Url->build(['controller'=>'Contents','action'=>'index'])
                        ],
                        'DoctorSchedules' => (object)[
                            'label' => 'Jadwal Praktek',
                            'icon' => 'flaticon-time',
                            'url' => $this->Url->build(['controller'=>'DoctorSchedules','action'=>'index'])
                        ],
                        'LinksMaps' => (object)[
                            'label' => 'Site Maps',
                            'icon' => 'la la-sitemap',
                            'url' => $this->Url->build(['controller'=>'LinksMaps','action'=>'index'])
                        ],
                        'Links' => (object)[
                            'label' => 'Link',
                            'icon' => 'la la-link',
                            'url' => $this->Url->build(['controller'=>'Links','action'=>'index'])
                        ],
                        'Covers' => (object)[
                            'label' => 'Covers',
                            'icon' => 'la la-image',
                            'url' => $this->Url->build(['controller'=>'Covers','action'=>'index'])
                        ],
                        'Albums' => (object)[
                            'label' => 'Albums',
                            'icon' => 'la la-image',
                            'url' => $this->Url->build(['controller'=>'Albums','action'=>'index'])
                        ],
                    ];
                    foreach($navList as $key => $nav):
                        if(array_key_exists($key,$sidebarList)):
                ?>
                        <li class="m-menu__item  <?=($this->request->controller==$key ? 'm-menu__item--active' : '');?>" aria-haspopup="true" >
                            <a  href="<?=$nav->url;?>" class="m-menu__link ">
                                <i class="m-menu__link-icon <?=$nav->icon;?>"></i>
                                <span class="m-menu__link-title">
                                    <span class="m-menu__link-wrap">
                                        <span class="m-menu__link-text">
                                            <?=$nav->label;?>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <?php endif;?>
                    <?php endforeach;?>
            <?php endif;?>
            <?php
                $countCheckSideBar = $this->Utilities->sideBarArrayCheck($sidebarList,['ContentsCategories','Contents','LinkMaps','Links']);
                if($countCheckSideBar > 0):
            ?>
                <li class="m-menu__section">
                    <h4 class="m-menu__section-text">
                        Masukan Masyarakat
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                <?php
                    $navList = (object)[
                        'PublicComplaints' => (object)[
                            'label' => 'Whistle Blowing System',
                            'icon' => 'flaticon-lifebuoy',
                            'url'  => $this->Url->build(['controller'=>'PublicComplaints','action'=>'index'])
                        ],
                        'ConflictInterests' => (object)[
                            'label' => 'Kepentingan Berbenturan',
                            'icon' => 'la la-image',
                            'url' => $this->Url->build(['controller'=>'ConflictInterests','action'=>'index'])
                        ],
                    ];
                    foreach($navList as $key => $nav):
                        if(array_key_exists($key,$sidebarList)):
                ?>
                        <li class="m-menu__item  <?=($this->request->controller==$key ? 'm-menu__item--active' : '');?>" aria-haspopup="true" >
                            <a  href="<?=$nav->url;?>" class="m-menu__link ">
                                <i class="m-menu__link-icon <?=$nav->icon;?>"></i>
                                <span class="m-menu__link-title">
                                    <span class="m-menu__link-wrap">
                                        <span class="m-menu__link-text">
                                            <?=$nav->label;?>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <?php endif;?>
                    <?php endforeach;?>
            <?php endif;?>
            <?php
                $countCheckSideBar = $this->Utilities->sideBarArrayCheck($sidebarList,['Groups','Users']);
                if($countCheckSideBar > 0):
            ?>
                <li class="m-menu__section">
                    <h4 class="m-menu__section-text">
                        Accessibility
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                <?php
                    $navList = (object)[
                        'Groups' => (object)[
                            'label' => 'Groups',
                            'icon' => 'flaticon-users',
                            'url'  => $this->Url->build(['controller'=>'Groups','action'=>'index'])
                        ],
                        'Users' => (object)[
                            'label' => 'Users',
                            'icon' => 'flaticon-user',
                            'url' => $this->Url->build(['controller'=>'Users','action'=>'index'])
                        ],
                    ];
                    foreach($navList as $key => $nav):
                        if(array_key_exists($key,$sidebarList)):
                ?>
                        <li class="m-menu__item  <?=($this->request->controller==$key ? 'm-menu__item--active' : '');?>" aria-haspopup="true" >
                            <a  href="<?=$nav->url;?>" class="m-menu__link ">
                                <i class="m-menu__link-icon <?=$nav->icon;?>"></i>
                                <span class="m-menu__link-title">
                                    <span class="m-menu__link-wrap">
                                        <span class="m-menu__link-text">
                                            <?=$nav->label;?>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <?php endif;?>
                    <?php endforeach;?>
            <?php endif;?>

            <?php
                $countCheckSideBar = $this->Utilities->sideBarArrayCheck($sidebarList,['AppSettings']);
                if($countCheckSideBar > 0):
            ?>
                <li class="m-menu__section">
                    <h4 class="m-menu__section-text">
                        Systems
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                <?php
                    $navList = (object)[
                        'AppSettings' => (object)[
                            'label' => 'APP Settings',
                            'icon' => 'flaticon-cogwheel',
                            'url' => $this->Url->build(['controller'=>'AppSettings','action'=>'index'])
                        ],
                        'WebSettings' => (object)[
                            'label' => 'Web Settings',
                            'icon' => 'flaticon-cogwheel',
                            'url' => $this->Url->build(['controller'=>'WebSettings','action'=>'index'])
                        ],
                    ];
                    foreach($navList as $key => $nav):
                        if(array_key_exists($key,$sidebarList)):
                ?>
                        <li class="m-menu__item  <?=($this->request->controller==$key ? 'm-menu__item--active' : '');?>" aria-haspopup="true" >
                            <a  href="<?=$nav->url;?>" class="m-menu__link ">
                                <i class="m-menu__link-icon <?=$nav->icon;?>"></i>
                                <span class="m-menu__link-title">
                                    <span class="m-menu__link-wrap">
                                        <span class="m-menu__link-text">
                                            <?=$nav->label;?>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <?php endif;?>
                    <?php endforeach;?>
            <?php endif;?>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->
