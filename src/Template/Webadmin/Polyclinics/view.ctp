<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Polyclinic $polyclinic
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Polyclinic'), ['action' => 'edit', $polyclinic->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Polyclinic'), ['action' => 'delete', $polyclinic->id], ['confirm' => __('Are you sure you want to delete # {0}?', $polyclinic->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Polyclinics'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Polyclinic'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Doctors'), ['controller' => 'Doctors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Doctor'), ['controller' => 'Doctors', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="polyclinics view large-9 medium-8 columns content">
    <h3><?= h($polyclinic->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Polyclinic') ?></th>
            <td><?= h($polyclinic->polyclinic) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($polyclinic->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Doctors') ?></h4>
        <?php if (!empty($polyclinic->doctors)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Photo') ?></th>
                <th scope="col"><?= __('Photo Dir') ?></th>
                <th scope="col"><?= __('Polyclinic Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Created By') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Modified By') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($polyclinic->doctors as $doctors): ?>
            <tr>
                <td><?= h($doctors->id) ?></td>
                <td><?= h($doctors->name) ?></td>
                <td><?= h($doctors->photo) ?></td>
                <td><?= h($doctors->photo_dir) ?></td>
                <td><?= h($doctors->polyclinic_id) ?></td>
                <td><?= h($doctors->created) ?></td>
                <td><?= h($doctors->created_by) ?></td>
                <td><?= h($doctors->modified) ?></td>
                <td><?= h($doctors->modified_by) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Doctors', 'action' => 'view', $doctors->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Doctors', 'action' => 'edit', $doctors->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Doctors', 'action' => 'delete', $doctors->id], ['confirm' => __('Are you sure you want to delete # {0}?', $doctors->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
