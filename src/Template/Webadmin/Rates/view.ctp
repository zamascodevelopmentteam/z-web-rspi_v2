<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <table class="table">
            <?php if(!empty($rate->parent_rate)):?>
            <tr>
                <th scope="row"><?= __('Kelompok') ?></th>
                <td><?= h($rate->parent_rate->name) ?></td>
            </tr>
            <?php endif;?>
            <tr>
                <th scope="row"><?= __('Tipe') ?></th>
                <td><?= h(($rate->type == 1 ? 'Kategori' : 'Item')) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Nama') ?></th>
                <td><?= h($rate->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($rate->id) ?></td>
            </tr>
            <?php if($rate->type == 2):?>
                <tr>
                    <th scope="row"><?= __('Unit') ?></th>
                    <td><?= h($rate->unit) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Harga Awal') ?></th>
                    <td><?= $this->Number->format($rate->price_1) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Harga Akhir') ?></th>
                    <td><?= $this->Number->format($rate->price_2) ?></td>
                </tr>
            <?php endif;?>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($rate->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($rate->modified) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Utilities->statusLabel($rate->status)?></td>
            </tr>
        </table>
        <!--end: Datatable -->
    </div>
</div>
