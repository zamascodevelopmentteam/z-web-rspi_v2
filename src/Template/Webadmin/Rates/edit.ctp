<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($rate,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('parent_id',['label'=>'Kelompok','options' => $parentRates, 'empty' => true]);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('type',['label'=>'Tipe','options' => [
                        1=>'Kategori Tarif','Item Tarif'
                    ]]);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('name',['label'=>'Nama']);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('status',['label'=>'Status','options'=>['DISABLED','ENABLED']]);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15 tarif-type">
                <div class="col-md-12">
                    <div class="row m--margin-bottom-15">
                        <div class="col-md-4">
                            <?=$this->Form->control('unit',['label'=>'Satuan']);;?>
                        </div>
                        <div class="col-md-4">
                            <?=$this->Form->control('price_1',['label'=>'Harga Awal']);;?>
                        </div>
                        <div class="col-md-4">
                            <?=$this->Form->control('price_2',['label'=>'Harga Akhir']);;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>
<?php $this->start('script');?>
    <script>
        function checkType(){
            var thisVal = $("#type").val();
            console.log(thisVal);
            if(thisVal == 1){
                $(".tarif-type").hide();
            }else{
                $(".tarif-type").show();
            }
        }
        checkType()
        $("#type").on("change",function(e){
            checkType()
        });
    </script>
<?php $this->end();?>