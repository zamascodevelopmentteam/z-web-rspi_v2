<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <button type="button" class="m-btn btn btn-secondary" data-toggle="modal" data-target="#m_modal_1">
                        <i class="la la-plus"></i> Tambah Folder
                    </button>
                </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="row m--margin-bottom-15">
            <div class="col-md-5 col-xl-3">
                <div id="m_tree_6" class="tree-demo"></div>
            </div>
            <div class="col-md-7 col-xl-9">
                <div class="file-manager">
                    <ul class="list-folder-zmc">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Create New Folder
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create($album,['class'=>'m-form m-form--fit m-form--label-align-right m-add-folder','url'=>[
                    'action' => 'add'
                ]]) ?>
                    <div class="row m--margin-bottom-15">
                        <div class="col-md-12">
                            <?=$this->Form->control('id',['type'=>'hidden','value'=>'']);?>
                            <?=$this->Form->control('status',['type'=>'hidden','value'=>1]);?>
                            <?=$this->Form->control('parent_id', ['type' => 'hidden','value'=>null, 'empty' => true]);?>
                            <?=$this->Form->control('title');?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary btn-submit-folder" >
                    Create
                </button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->

<div class="modal fade" id="m_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Upload Files
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create(null,['class'=>'m-form m-form--fit m-form--label-align-right m-add-files','url'=>[
                    'action' => 'uploadFiles'
                ],
                'type' => 'file']) ?>
                <?=$this->Form->control('album_id',['type'=>'hidden','value'=>'']);?>
                    <div class="row m--margin-bottom-15">
                        <div class="col-md-12">
                            <div class="m-dropzone dropzone m-dropzone--primary" action='<?=$this->Url->build(['action'=>'uploadFiles']);?>'  id="m-dropzone-two">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 class="m-dropzone__msg-title">
                                        Drop files here or click to upload.
                                    </h3>
                                    <span class="m-dropzone__msg-desc">
                                        Upload up to 10 files
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->


<?php $this->start('script');?>
<script>
    var idParent = null;

    var tree_folder = $("#m_tree_6").jstree({
        "core" : {
            "themes" : {
                "responsive": false
            }, 
            "deselect_all" : true,
            // so that create works
            "check_callback" : true,
            'data' : {
                'url' : function (node) {
                    return '<?=$this->Url->build(['action'=>'index']);?>';
                },
                'dataType' : 'json',
                'data' : function (node) {
                    if(node.id == "#"){
                        return {'parent_id' : ''};
                    }
                    return { 'parent_id' : node.id };
                }
            }
        },
        "types" : {
            "default" : {
                "icon" : "fa fa-folder m--font-brand"
            },
            "file" : {
                "icon" : "fa fa-file  m--font-brand"
            }
        },
        "state" : { "key" : "demo3" },
        "plugins" : [ "dnd", "types" ]
    }).on('select_node.jstree', function (e, data) {
        var i, j, r = [];
        var id = data.instance.get_node(data.node, true).attr('id');
        if(id == "j1_1"){
            id = null;
        }
        idParent = id;
        var node;
        $("#m_tree_6").jstree(true).open_node(data.instance.get_node(data.node, true));
        getFolder(id)
    }).bind("move_node.jstree", function(e, data) {
        console.log("Drop node " + data.node.id + " to " + data.parent);
        $.ajax({
            url : "<?=$this->Url->build(['action'=>'add']);?>",
            data : {
                id : data.node.id,
                parent_id : data.parent,
            },
            type : 'post',
            dataType : "json",
            success : function(result){

            }
        })
    });;
    getFolder(null);
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };
    function getFolder(id){
        $.ajax({
            url : '<?=$this->Url->build(['action'=>'getFolder']);?>',
            data : {'parent_id' : id},
            dataType : 'json',
            beforeSend : function(){

            },
            success : function(result){
                var html = '';
                $.each(result.dataFolder,function(e,item){
                    html += '<li>'
                            +'<div class="tools">'
                            +'<a href="#" class="edit editFolder" data-node-id="'+item.id+'" data-parent-id="'+item.parent_id+'" data-text="'+item.text+'">'
                            +'<i class="fa fa-pencil"></i>'
                            +'</a>'
                            +'<a href="#" class="delete deleteFolder" data-node-id="'+item.id+'" data-parent-id="'+item.parent_id+'" data-text="'+item.text+'">'
                            +'<i class="fa fa-times m--font-danger"></i>'
                            +'</a>'
                            +'</div>'
                            +'<a href="#" class="item getFolder" data-node-id="'+item.id+'">'
                            +'<div class="image">'
                            +'<i class="fa fa-folder icon-lg m--font-warning"></i>'
                            +'</div>'
                            +'<div class="caption">'
                            +'<p>'+item.text+'</p>'
                            +'</div>'
                            +'</a>'
                            +'</li>'
                })
                
                $.each(result.dataFile,function(e,item){
                    var image = "";
                    var fileDir = item.file_dir;
                    fileDir = fileDir.replaceAll("webroot","");
                    fileDir = fileDir.replace(/\\/g, "/");
                    var file = "<?=$this->request->base;?>"+fileDir+"/thumbnail-"+item.file;
                    var originfile = "<?=$this->request->base;?>"+fileDir+"/"+item.file;
                    if(item.file_type != "application/pdf"){
                        image = "<img src='"+file+"'>";
                        var file_type = 'image';
                        var docType = '';
                        
                    }else{
                        var docType = "<?=$this->request->base;?>/assets/webadmin/img/pdf-icon.png";
                        image = "<img src='"+docType+"'>";
                        var file_type = 'pdf';
                        
                    }
                    html += '<li>'
                            +'<div class="tools">'
                            +'<a href="#" class="delete deleteFile" data-node-id="'+item.id+'" data-parent-id="'+item.album_id+'" data-text="'+item.text+'">'
                            +'<i class="fa fa-times m--font-danger"></i>'
                            +'</a>'
                            +'</div>'
                            +'<a href="'+originfile+'" class="item viewFile" data-node-id="'+item.id+'" data-type="'+file_type+'" data-caption="'+item.file+'" data-doc-url="'+docType+'">'
                            +'<div class="image">'
                            + image
                            +'</div>'
                            +'<div class="caption">'
                            +'<p>'+item.file+'</p>'
                            +'</div>'
                            +'</a>'
                            +'</li>'
                })
                $(".list-folder-zmc").html(html);
                var toolsheader = $(".m-portlet__head-tools").find(".btn-group");
                if(id != null && id != ""){
                    if(toolsheader.find('.btn-upload').length == 0){
                        toolsheader.append('<button type="button" class="m-btn btn btn-secondary btn-upload" data-toggle="modal" data-target="#m_modal_2"><i class="la la-upload"></i> Upload File</button>');
                    }
                    
                }else{
                    toolsheader.find('.btn-upload').remove();
                }
                
            }
        })
    }
    $("body").on("click",".editFolder",function(e){
        e.preventDefault();
        idParent = $(this).data('parentId');
        $(".m-add-folder #id").val($(this).data('nodeId'));
        $(".m-add-folder #title").val($(this).data('text'));
        $('#m_modal_1').modal('show');
    })
    $("body").on("click",".getFolder",function(e){
        e.preventDefault();
        idParent = $(this).data('nodeId');
        getFolder(idParent);
        var nodeTree = $(".jstree-node#"+idParent);
        tree_folder.one("refresh_node.jstree", function () { tree_folder.jstree(true).open_node(nodeTree); })
        tree_folder.jstree(true).refresh_node(nodeTree);
        tree_folder.jstree(true).select_node(nodeTree);
    })
    $("body").on("click",".deleteFolder",function(e){
        e.preventDefault();
        idParent = $(this).data('parentId');
        nodeId = $(this).data('nodeId');
        var thisUrl = "<?=$this->Url->build(['action'=>'delete']);?>/"+nodeId
        swal({
            title: 'Are you sure?',
            text: 'Please make sure if you want to delete this folder',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url : thisUrl,
                    type : "delete",
                    dataType : "json",
                    beforeSend : function(){
                        swal('Please Wait', 'Requesting Data', 'info')
                    },
                    success : function(result){
                        if(result.code == 200){
                            swal(
                                'Success',
                                result.message,
                                'success'
                            )
                            if(idParent == '' || idParent == null || idParent == 0){
                                var nodeTree = $(".jstree-node#j1_1");
                            }else{
                                var nodeTree = $(".jstree-node#"+idParent);
                            }
                            getFolder(idParent);
                            tree_folder.one("refresh_node.jstree", function () { tree_folder.jstree(true).open_node(nodeTree); })
                            tree_folder.jstree(true).refresh_node(nodeTree);
                        }else if(result.code == 99){
                            swal("Ooopp!!!", result.message,"error");
                        }else{
                            swal("Ooopp!!!", result.message,"error");
                        }
                    },
                    error : function()
                    {
                        swal("Ooopp!!!","Failed to deleted folder, please try again","error");
                    }
                })
            // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            } else if (result.dismiss === 'cancel') {
                
            }
        })
    })
    $("body").on("click",".deleteFile",function(e){
        e.preventDefault();
        nodeId = $(this).data('nodeId');
        var thisLi = $(this).closest('li');
        var thisUrl = "<?=$this->Url->build(['action'=>'deleteFile']);?>/"+nodeId
        swal({
            title: 'Are you sure?',
            text: 'Please make sure if you want to delete this file',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url : thisUrl,
                    type : "delete",
                    dataType : "json",
                    beforeSend : function(){
                        swal('Please Wait', 'Requesting Data', 'info')
                    },
                    success : function(result){
                        if(result.code == 200){
                            swal(
                                'Success',
                                result.message,
                                'success'
                            )
                            thisLi.remove();
                        }else if(result.code == 99){
                            swal("Ooopp!!!", result.message,"error");
                        }else{
                            swal("Ooopp!!!", result.message,"error");
                        }
                    },
                    error : function()
                    {
                        swal("Ooopp!!!","Failed to deleted folder, please try again","error");
                    }
                })
            // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
            } else if (result.dismiss === 'cancel') {
                
            }
        })
    })
    $('#m_modal_1').on('shown.bs.modal', function (e) {
        $(".m-add-folder #parent-id").val(idParent);
    })
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#m-dropzone-two", {
        paramName: "file", // The name that will be used to transfer the file
        maxFiles: 10,
        maxFilesize: 200, // MB
        addRemoveLinks: true,
        acceptedFiles: "image/*,application/pdf",
        accept: function(file, done) {
            done();
        },   
        init: function() {
            this.on("processing", function(file) {
                this.options.url = "<?=$this->Url->build(['action'=>'uploadFiles']);?>/"+idParent
            });
            var cd;
            this.on("success", function(file, response) {
                $('.dz-progress').hide();
                $('.dz-size').hide();
                $('.dz-error-mark').hide();
                $('.dz-remove').hide();
                console.log(response);
                console.log(file);
                cd = response;
            });
        },
        complete : function(){
            if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                getFolder(idParent);
            }
        }
    });
    $('#m_modal_2').on('show.bs.modal', function (e) {
        $(".m-add-files #album-id").val(idParent);
        console.log(myDropzone);
    })
    $('#m_modal_2').on('hidden.bs.modal', function (e) {
        $(".m-add-files #album-id").val(idParent);
        myDropzone.removeAllFiles(true)
    })

    
    $('#m_modal_1').on('hidden.bs.modal', function (e) {
        $(".m-add-folder #id").val('');
    })
    $( "body" ).on("contextmenu",".getFolder",function(e) {
        e.preventDefault();
        alert( "Handler for .contextmenu() called." );
    });
    $(".btn-submit-folder").on("click",function(e){
        e.preventDefault()
        $(".m-add-folder").submit();
    });
    $(".btn-submit-files").on("click",function(e){
        e.preventDefault()
        $(".m-add-files").submit();
    });
    $(".m-add-folder").on("submit",function(e){
        e.preventDefault();
        $(this).ajaxSubmit({
            dataType : 'json',
            success: function(res) {
                console.log(res);
                if(res.code == 200){
                    $('#m_modal_1').modal('hide');
                    $('#m_modal_1 #title').val("");
                    if(idParent == '' || idParent == null || idParent == 0){
                        var nodeTree = $(".jstree-node#j1_1");
                    }else{
                        var nodeTree = $(".jstree-node#"+idParent);
                    }
                    console.log(nodeTree);
                    getFolder(idParent);
                    tree_folder.one("refresh_node.jstree", function () { tree_folder.jstree(true).open_node(nodeTree); })
                    tree_folder.jstree(true).refresh_node(nodeTree);
                }else{
                    swal('Oopps!!','Terjadi kesalahan','error');
                }
            }
        });
    })
    $(".m-add-files").on("submit",function(e){
        e.preventDefault();
        $(this).ajaxSubmit({
            dataType : 'json',
            success: function(res) {
                console.log(res);
                if(res.code == 200){
                    $('#m_modal_2').modal('hide');
                    $('#m_modal_2 #title').val("");
                    if(idParent == '' || idParent == null || idParent == 0){
                        var nodeTree = $(".jstree-node#j1_1");
                    }else{
                        var nodeTree = $(".jstree-node#"+idParent);
                    }
                    console.log(nodeTree);
                    getFolder(idParent);
                    tree_folder.one("refresh_node.jstree", function () { tree_folder.jstree(true).open_node(nodeTree); })
                    tree_folder.jstree(true).refresh_node(nodeTree);
                }else{
                    swal('Oopps!!','Terjadi kesalahan','error');
                }
            },
            error : function(){
                console.log('error');
            }
        });
    })
</script>
<?php $this->end();?>