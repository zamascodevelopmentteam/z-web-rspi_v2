<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <table class="table">
        <tr>
            <th scope="row"><?= __('Nama') ?></th>
            <td><?= $conflictInterest->name ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= $conflictInterest->email ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Alamat') ?></th>
            <td><?= $conflictInterest->address ?></td>
        </tr> 
        <tr>
            <th scope="row"><?= __('No. Telpon') ?></th>
            <td><?= $conflictInterest->phone ?></td>
        </tr>     
        <tr>
            <th scope="row"><?= __('Pesan') ?></th>
            <td><?= $conflictInterest->message ?></td>
        </tr>
        <tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($conflictInterest->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($conflictInterest->created) ?></td>
        </tr>
        </table>
        <!--end: Datatable -->
    </div>
</div>
