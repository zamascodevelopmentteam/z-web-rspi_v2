<?php
    $rightButton = '';
?>
<?=$this->element('widget/index',['rightButton' => $rightButton]);?>
<?php
    $deleteUrl    = $this->Url->build(['action'=>'delete'])."/";
    if($this->Acl->check(['action'=>'delete']) == false){
        $deleteUrl = "";
    }
    $editUrl      = $this->Url->build(['action'=>'edit'])."/";
    if($this->Acl->check(['action'=>'edit']) == false){
        $editUrl = "";
    }
    $viewUrl      = $this->Url->build(['action'=>'view'])."/";
    if($this->Acl->check(['action'=>'view']) == false){
        $viewUrl = "";
    }
?>
<?=$this->Html->scriptBlock('
jQuery(document).ready(function() {
    var deleteUrl = "'.$deleteUrl.'";
    var editUrl = "'.$editUrl.'";
    var viewUrl = "'.$viewUrl.'";
    var columnData = [{
        field: "ConflictInterests.id",
        title: "ID",
        sortable: false,
        width: 40,
        selector: false,
        textAlign: "center",
        template: function(t) {
            return t.id
        }
    },  {
        field: "ConflictInterests.name",
        title: "Name",
        sort : \'asc\',
        template: function(t) {
            return t.name
        }
    },  {
        field: "ConflictInterests.email",
        title: "Email",
        sort : \'asc\',
        template: function(t) {
            return t.email
        }
    },  {
        field: "ConflictInterests.created",
        title: "Created",
        sort : \'asc\',
        template: function(t) {
            return Utils.dateIndonesia(t.created,true,true)
        }
    },  {
        field: "ConflictInterests.created",
        title: "Created",
        sort : \'asc\',
        template: function(t) {
            return Utils.dateIndonesia(t.created,true,true)
        }
    },  {
        field: "ConflictInterests.modified",
        title: "Modified",
        sort : \'asc\',
        template: function(t) {
            return Utils.dateIndonesia(t.modified,true,true)
        }
    },
    {
        field: "actions",
        width: 100,
        title: "Actions",
        sortable: false,
        overflow: "visible",
        template: function(t) {
            var btn =  \'<div class="dropdown"><a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown"><i class="la la-ellipsis-h"></i></a>  <div class="dropdown-menu dropdown-menu-right">\'
            var btnList = \'\';
            if(viewUrl != ""){
                btnList += \'<a class="dropdown-item" href="\'+viewUrl+t.id+\'"><i class="flaticon-search-1"></i> View</a>\';
            }
            if(deleteUrl != ""){
                btnList += \'<a class="btn-delete-on-table dropdown-item" href="\'+deleteUrl+t.id+\'"><i class="flaticon-cancel"></i> Delete</a>\';
            }
            if(editUrl != ""){
                btnList += \'<a class="dropdown-item" href="\'+editUrl+t.id+\'"><i class="flaticon-edit"></i> Edit</a>\';
            }
            
            if(btnList == ""){
                btn = "";
            }else{
                btn += btnList;
                btn += \'</div></div>\';
            }
            return btn;
        }
    }];
    DatatableRemoteAjax.init("",columnData)
});',['block'=>'script']);