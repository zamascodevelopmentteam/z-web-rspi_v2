<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($link,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('title');;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('links_map_id', ['options' => $linksMaps,'empty'=>'Choose One']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('parent_id', ['options' => $parents,'empty'=>'Choose One']);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('_type', ['options' => ['Contents'=>'CONTENTS','External'=>'EXTERNAL LINK']]);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('target',['options'=>['_blank'=>'_BLANK', '_self'=>'_SELF', '_parent'=>'_PARENT', '_top'=>'_TOP']]);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('content_id', ['options' => $listContents,'empty'=>'Choose One']);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->control('meta_description');?>
                </div>
                <div class="col-md-6">
                    <?=$this->Form->control('meta_keyword');?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('url',['label'=>'URL']);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('status',['options'=>['DISABLED','ENABLED'],'default'=>0]);?>
                </div>
                <div class="col-md-4 col-lg-2">
                    <?=$this->Form->control('sort');;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->control('picture',[
                        'label' => '* Picture',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);?>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        var jsonContents = <?=$contents;?>;
        $("#links-map-id").on("change",function(e){
            var valThis = $("#links-map-id").val();
            console.log(valThis);
            var html = "<option value=''>Pilih Parent</option>";
            if(valThis.length > 0){
                $.ajax({
                    url : "<?=$this->Url->build(['controller' => 'Apis','action' => 'getLinkBySiteMap']);?>/" + valThis,
                    dataType : 'json',
                    type : 'get',
                    success : function(result){
                        $.each(result,function(e,i){
                            html += "<option value="+e+">"+i+"</option>";
                        })
                        $("#parent-id").html(html);
                    }
                })
            }else{
                $("#parent-id").html(html);
            }
            
        })
        function changeType(){
            var type = $("#type").val();
            if(type == "Contents"){
                $("#content-id").prop("disabled",false);
                $("#url").prop("disabled",true);
                $("#meta-description").prop("disabled",true);
                $("#meta-keyword").prop("disabled",true);
                $("#url").prop("disabled",true);
            }else{
                $("#content-id").prop("disabled",true);
                $("#content-id").val("");
                $("#url").prop("disabled",false);
                $("#meta-description").prop("disabled",false);
                $("#meta-keyword").prop("disabled",false);
            }
        }
        changeType();
        $("#type").on("change",function(e){
            changeType();    
        });
        $("#content-id").on("change",function(e){
            if($(this).val() != ""){
                $("#url").val(jsonContents[$(this).val()]);
            }else{
                $("#url").val("");
            }
        });
    </script>
<?php $this->end();?>