<header>
    <section class="top-header">
        <section class="container">
            <div class="row">
                <div class="col-md-12 col-lg-3 main-header">
                    <div class="logo-container">
                        <a href="#" class="logo-header">
                            <img src="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['Web.Logo']);?>">
                        </a>
                    </div>
                    <div class="button-nav-mobile">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="fa fa-bars"></span>
                    </button>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 d-none d-sm-block">
                    <div class="widget-header">
                        <div class="widget-header-icon">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="widget-header-caption">
                            <div class="widget-header-caption-title">
                                Alamat :
                            </div>
                            <div class="widget-header-caption-content">
                                <?=$defaultAppSettings['Web.Location.Address'];?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 d-none d-sm-block">
                    <div class="widget-header">
                        <div class="widget-header-icon">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <div class="widget-header-caption">
                            <div class="widget-header-caption-title">
                                Email :
                            </div>
                            <div class="widget-header-caption-content">
                                <?=$defaultAppSettings['Web.Location.Email'];?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 d-none d-sm-block">
                    <div class="widget-header">
                        <div class="widget-header-icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="widget-header-caption">
                            <div class="widget-header-caption-title">
                                No. Telepon :
                            </div>
                            <div class="widget-header-caption-content">
                                <?=$defaultAppSettings['Web.Location.Phone.Number'];?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <section class="nav-header">
        <section class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto ml-auto">
                        <?php 
                            $generateLinkHeader = $this->Utilities->generateLinkHeader($links['HEADER']);
                            echo $generateLinkHeader;
                        ?>
                    </ul>
                </div>
            </nav>
        </section>
    <section>
</header>