<footer>
    <section class="container">
        <div class="row" style="margin-bottom: 30px;">   
            <div class="col-md-4 col-sm-12 col-xs-12" style="margin-bottom: 20px;">
                <div class="widget-header">
                    <div class="widget-header-icon">
                        <i class="fa fa-map-marker"></i>
                    </div>
                    <div class="widget-header-caption">
                        <div class="widget-header-caption-title">
                            Alamat :
                        </div>
                        <div class="widget-header-caption-content">
                            <?=$defaultAppSettings['Web.Location.Address'];?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12" style="margin-bottom: 20px;">
                <div class="widget-header">
                    <div class="widget-header-icon">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="widget-header-caption">
                        <div class="widget-header-caption-title">
                            No. Telepon :
                        </div>
                        <div class="widget-header-caption-content">
                            <?=$defaultAppSettings['Web.Location.Phone.Number'];?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12" style="margin-bottom: 20px;">
                <div class="widget-header">
                    <div class="widget-header-icon">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="widget-header-caption">
                        <div class="widget-header-caption-title">
                            Email :
                        </div>
                        <div class="widget-header-caption-content">
                            <?=$defaultAppSettings['Web.Location.Email'];?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-10" style="margin-bottom: 20px;">
                <i class="fa fa-copyright"></i> 2018 <a href="<?=$this->request->base;?>/">RSPI SULIANTI SAROSO</a> 
            </div>
            <div class="col-md-8 col-xs-2" style="margin-bottom: 20px;">
                <a href="#" class="to-top-nav"><i class="fa fa-angle-double-up"></i></a> 
            </div>
        </div>
    </section>
</footer>