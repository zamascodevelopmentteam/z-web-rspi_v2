<?=$this->element('header');?>
<main class="main">
    <?php if(!empty($cover)):?>
        <section class="cover-page">
            <section class="owl-carousel cover-owl">
                <?php foreach($cover as $cov):?>
                    <div class="item">
                        <?php $image = $this->Utilities->generateUrlImage($cov->file_dir,$cov->file,'thumbnail-');?>
                        <img src="<?=$image;?>">
                    </div>
                <?php endforeach;?>
            </section>
        </section>
    <?php endif;?>
    <?=$this->fetch('content');?>
</main>
<?=$this->element('footer');?>
