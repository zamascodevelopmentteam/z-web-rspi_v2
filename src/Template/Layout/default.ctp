
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$defaultAppSettings['Web.Name'];?><?=(!empty($this->fetch('title')) ? ' | '.$this->fetch('title') : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$this->fetch('meta_description');?>">
    <meta name="keywords" content="<?=$this->fetch('meta_keyword');?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="<?=$this->request->base;?>/assets/img/favico.png?v2" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=$this->request->base;?>/assets/img/apple-touch-icon-144x144-precomposed.png?v2">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?=$this->request->base;?>/assets/img/apple-touch-icon-120x120-precomposed.png?v2">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=$this->request->base;?>/assets/img/apple-touch-icon-114x114-precomposed.png?v2">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=$this->request->base;?>/assets/img/apple-touch-icon-72x72-precomposed.png?v2">
    <link rel="apple-touch-icon-precomposed" href="<?=$this->request->base;?>/assets/img/apple-touch-icon-57x57-precomposed.png?v2">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700|Quicksand:400,700" rel="stylesheet">
    <meta name="theme-color" content="#0564ff" />
    <?php
        $cssDefault = [
            'dist/css/vendor.bundle.css',
        ];

        $cssMain = [

        ];

        $this->Html->css($cssDefault,['block'=>'cssDefault','pathPrefix' => '/assets/']);
        $this->Html->css($cssMain,['block'=>'cssMain','pathPrefix' => '/assets/']);

        echo $this->fetch('cssExternal');
        echo $this->fetch('cssDefault');
        echo $this->fetch('cssPlugin');
        echo $this->fetch('cssMain');
    ?>

</head>
    <body class="">
        <?=$this->element('main');?>
        
        <?php
            $jsDefault = [
              'dist/js/vendor.bundle.js',
            ];
            $jsMain = [

            ];
            $this->Html->script($jsDefault,['block'=>'jsDefault','pathPrefix' => '/assets/']);
            $this->Html->script($jsDefault,['block'=>'jsPlugin','pathPrefix' => '/assets/']);
            $this->Html->script($jsMain,['block'=>'jsMain','pathPrefix' => '/assets/']);

            echo $this->fetch('jsDefault');
            echo $this->fetch('jsPlugin');
            echo $this->fetch('jsMain');
            echo $this->fetch('script');
        ?>
        <script>
            var itemOwl = $(".cover-owl").find(".item");
            var headerHeight = $("header").innerHeight();
            var footerHeight = $("footer").innerHeight();
            var bodyHeight = $(window).height();
            var mainHeight = bodyHeight - (headerHeight*1 + footerHeight*1);
            $("main").css({
                "min-height" : mainHeight
            });
            if(itemOwl.length >= 1){
                $('.cover-owl').owlCarousel({
                    navigation : true, 
                    items : 1,
                    slideSpeed : 400,
                    paginationSpeed : 400,
                    singleItem: true,
                    pagination: false,
                    autoplay : true,
                    autoplaySpeed:400,
                    nav : true,
                    loop : true,
                    navText: [
                        "<i class='fa fa-caret-left'></i>",
                        "<i class='fa fa-caret-right'></i>"
                    ],
                    dots : false
                });
            }

            var myVideo = document.getElementById("video-compro");
            $(".play-icon").on("click",function(e){
                e.preventDefault();
                $(".bg-video").addClass("play");
                myVideo.play();
                $("body").addClass("bplay-video");
            })

            $("body").on("click",".video-pause",function(e){
                e.preventDefault();
                myVideo.pause();
                $(this).attr("class","video-play");
                $(this).find("i").attr("class","fa fa-play");
            });
            $("body").on("click",".video-play",function(e){
                e.preventDefault();
                myVideo.play();
                $(this).attr("class","video-pause");
                $(this).find("i").attr("class","fa fa-pause");
            });
            $(".video-stop").on("click",function(e){
                e.preventDefault();
                myVideo.pause();
                myVideo.currentTime = 0;
                $(".video-pause").attr("class","video-play");
                $(".video-play").find("i").attr("class","fa fa-play");
            })
            $(".video-close").on("click",function(e){
                e.preventDefault();
                $(".bg-video").removeClass("play");
                myVideo.pause();
                myVideo.currentTime = 0;
                $("body").removeClass("bplay-video");
            })
            $(".to-top-nav").on("click",function(e){
                e.preventDefault();
                $('html,body').animate({ scrollTop: 0 }, 'fast');
            })
        </script>
    </body>
</html>
