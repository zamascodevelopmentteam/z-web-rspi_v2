<?php
namespace App\View\Helper;

use Cake\View\Helper;

class UtilitiesHelper extends Helper
{
    public $helpers = ['Url','Number'];
    public function sideBarArrayCheck( array $array, $keys ) {
        $count = 0;
        if ( ! is_array( $keys ) ) {
            $keys = func_get_args();
            array_shift( $keys );
        }
        foreach ( $keys as $key ) {
            if ( isset( $array[$key] ) || array_key_exists( $key, $array ) ) {
                $count ++;
            }
        }

        return $count;
    }

    public function labelSettings($label){
        return str_replace(".","",$label);
    }

    public function statusLabel($label){
        if($label){
            return "<span class=\"m-badge m-badge--brand m-badge--wide\"> ENABLED</span>";
        }else{
            return "<span class=\"m-badge m-badge--danger m-badge--wide\"> DISABLED</span>";
        }
    }

    public function generateUrlImage($img_dir=null,$img,$prefix = null,$img_not_found = null)
    {
		//full_path_dir
        $baseDir = "";
        if($img_dir == null){
            $img_dir = $img;
            if(substr($img_dir,0,1) == "/" || substr($img_dir,0,1) == "\""){
                $img_dir = substr($img_dir,1);
            }
            $changeSlash 		= str_replace("\\",DS,$img_dir);
            $changeSlash		= str_replace("/",DS,$changeSlash);
            $changeSlash        = str_replace("webroot\\","",$changeSlash);
            $changeSlash        = str_replace("webroot/","",$changeSlash);
            $full_path = WWW_ROOT.$changeSlash;
            $noDir = true;
        }else{
            $img_dir = $baseDir.$img_dir."/";
            $img = $prefix.$img;
            $changeSlash 		= str_replace("\\",DS,$img_dir);
            $changeSlash		= str_replace("/",DS,$changeSlash);
            $full_path = ROOT.DS.$changeSlash.$img;
            $noDir = false;
        }

		//check image exist
		if(file_exists($full_path)){
            if($noDir == false){
                $dir 		= str_replace("\\","/",$img_dir).$img;
            }else{
                $dir 		= str_replace("\\","/",$img_dir);
            }

			$dir = str_replace("webroot/","",$dir);
			$url = $this->Url->build("/".$dir,true);
		}else{
			if($img_not_found == null){
				$url = $this->Url->build("/img/no-image.png",true);
			}else{
				$url = $this->Url->build("/".$img_not_found,true);
			}
		}
		return $url;
    }

    public function monthArray($key = null)
    {
        $array = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        if($key == null){
            return $array;
        }else{
            return $array[$key];
        }

    }

    public function monthShortArray($key = null)
    {
        $array = array (1 =>   'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'Mei',
                    'Jun',
                    'Jul',
                    'Ags',
                    'Sep',
                    'Okt',
                    'Nov',
                    'Des'
                );
        if($key == null){
            return $array;
        }else{
            return $array[$key];
        }

    }

    public function indonesiaDateFormat($tanggal,$time = false,$toIndonesia = true)
    {
        $bulan = $this->monthArray();
        $datepick = explode(" ", $tanggal);
        $split = explode("-", $datepick[0]);
        if($toIndonesia == true){
            if($time == false){
                return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
            }else{
                return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0]. ' ' . $datepick[1];
            }
        }else{
            if($time == false){
                return $split[2] . '-' . $split[1]. '-' . $split[0];
            }else{
                return $split[2] . '-' . $split[1] . '-' . $split[0].' '.$datepick[1];
            }
        }

    }

    public function generateLinkHeader($links,$level = 0,$type = '')
    {
        $listMenus = "";
        foreach($links as $key => $r){
            $url = $this->generateUrlMenu($r);
            if($level == 0){
                if(!empty($r['children'])){
                    $listMenus .= '<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink'.$r['id'].'" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        '.$r['title'].'
                    </a><div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink'.$r['id'].'">';
                    $listMenus .= $this->generateLinkHeader($r['children'],$level + 1);
                    $listMenus .= '</div></li>';
                }else{
                    $listMenus .= "<li class='nav-item'><a href='".$url."' class='nav-link'  target='".$r['target']."'>".$r['title']."</a></li>";
                }
            }
            if($level >= 1){
                if(!empty($r['children'])){
                    $listMenus .= '<a class="nav-link dropdown-toggle" href="#">
                        '.$r['title'].'
                    </a>';
                    $listMenus .= $this->generateLinkHeader($r['children'],$level + 1);
                    $listMenus .= '</ul></li>';
                }else{
                    $listMenus .= "<a href='".$url."' class='dropdown-item'  target='".$r['target']."'>".$r['title']."</a>";
                }
            }
        }
        return $listMenus;
    }

    public function generateLinkHeaderMobile($links,$level = 0,$type = '')
    {
        $listMenus = "";
        foreach($links as $key => $r){
            $url = $this->generateUrlMenu($r);
            if($level == 0){
                if(!empty($r['children'])){
                    $listMenus .= '<li>
                    <a>
                        '.$r['title'].'
                    </a><ul class="list-unstyled">';
                    $listMenus .= '<li>' .$this->generateLinkHeaderMobile($r['children'],$level + 1).'</li>';
                    $listMenus .= '</ul></li>';
                }else{
                    $listMenus .= "<li><a href='".$url."' target='".$r['target']."'>".$r['title']."</a></li>";
                }
            }
            if($level >= 1){
                if(!empty($r['children'])){
                    $listMenus .= '<a>'.$r['title'].'</a>';
                    $listMenus .= '<ul class="list-unstyled">';
                    $listMenus .= $this->generateLinkHeader($r['children'],$level + 1);
                    $listMenus .= "</ul>";
                }else{
                    $listMenus .= '<a href="'.$url.'">'.$r['title'].'</a>';
                }
            }
        }
        return $listMenus;
    }

    public function generateLinkFooter($links,$level = 0){
        $html = "";
        foreach($links as $key => $link){
            $url = $this->generateUrlMenu($link);
            if($level == 0){
                if(empty($link['children'])){
                    $html .= "<div class='col-md-4'>";
                    $html .= "<li><i class='fa fa-angle-right'></i><a href='".$url."' target='".$link['target']."'>".$link['title']."</a></li>";
                    $html .= "</div>";
                }else{
                    $html .="<div class='col-md-4'>";
                    $html .="<div class='qlink'>";
                    $html .= "<h4>".$link['title']."</h4>";
                    $html .= "<ul class='list-unstyled'>";
                    $html .= "<li>".$this->generateLinkFooter($link['children'],$level + 1)."</li>";
                    $html .= "</ul>";
                    $html .= "</div>";
                    $html .= "</div>";
                }
            }else{
                if(empty($link['children'])){
                    $html .="<li>";
                    $html .= "<a href='".$url."' target='".$link['target']."'>".$link['title']."</a>";
                    $html .= "</li>";
                }else{
                    $html .="<li class='has-child'>";
                    $html .= $link['title'];
                    $html .= "<ul>";
                    $html .= $this->generateLinkFooter($link['children'],$level + 1);
                    $html .= "</ul>";
                    $html .= "</div>";
                }
            }

        }
        return $html;
    }

    public function generateTarif($tarif,$num = null){
        $tr = "";
        $no = 1;
        foreach($tarif as $key => $r){
            $classTr = '';
            if(!empty($r->children)){
                $classTr= "tr-bold";
            }
            $tr .= "<tr class='".$classTr."'>";
            $tr .= "<td width='5%'>".$num.$no."</td>";
            $tr .= "<td>".$r->name."</td>";
            $tr .= "<td width='15%'>".$r->unit."</td>";
            $tr .= "<td width='10%' class='text-right'>".(!empty($r->price_1) ? $this->Number->precision($r->price_1,2): '&nbsp;')."</td>";
            $tr .= "<td width='5%' class='text-center'>".(!empty($r->price_1) && !empty($r->price_2) ? 'sd' : '' )."</td>";
            $tr .= "<td width='10%' class='text-right'>".(!empty($r->price_2) ? $this->Number->precision($r->price_2,2) : '&nbsp;')."</td>";
            $tr .= "</tr>";
            if(!empty($r->children)){
                if($num == null){
                    $ac = $num.$no.'.';
                }else{
                    $ac = $num.$no.'.';
                }
                
                $tr .= $this->generateTarif($r->children,$ac);
            }
            $no ++;
        }
        return $tr;
    }

    public function generateUrlMenu($link)
    {
        $url = "";
        if($link['_type']=="Contents"):
            $url = $this->Url->build(['controller'=>'Pages','action'=>'display','slug'=>$link['url']]);
        else:
            if(filter_var($link['url'], FILTER_VALIDATE_URL) == FALSE){
                $url = $this->Url->build("/".$link['url']);
            }else{
                $url = $link['url'];
            }
        endif;
        return $url;
    }
}
