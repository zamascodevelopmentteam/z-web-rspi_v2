
# Z-WEBadmin 

### REQUIREMENT
#### **WEBSERVICE**
1. HTTP Server. For example: Apache. Having mod_rewrite is preferred, but by no means required.
2. PHP 5.6.0 or greater (including PHP 7.1).
3. mbstring PHP extension
4. intl PHP extension
5. simplexml PHP extension 

#### **DATABASE** Choose one
1. MySQL (5.1.10 or greater)
2. PostgreSQL
3. Microsoft SQL Server (2008 or higher)
4. SQLite 3

#### **DEVELOPMENT ASSETS** 
1. NPM
2. BOWER
3. YARN
4. GULP
  
----------

### INSTALATION
1. Setelah melakukan clone harap melakukan comand composer install
2. copy file config/app.default.php > config/app.php
3. export database di folder config/schema/zmc_website.sql
4. hapus remote GIT dengan perintah `git remote remove origin`
5. daftarkan repo git website dengan perintah `git remote add origin url-repo`
#### DEVELOPMENT ASSETS
PLEASE CHECK URL BELLOW FOR INSTALATION BUILD TOOLS DEVELOPMENT ASSET : [METRONIC DOCUMENTATION](https://keenthemes.com/metronic/documentation.html)

----------
### DEVELOPMENT STEP 
1. **Z-WEBadmin** menggukana system ***ACL*** atau bisa disebut dengan ***Access Control List***. ***ACL*** terbagi menjadi dua bagian yaitu ***AROS*** dan ***ACOS***.  Harap baca url berikut untuk memahami ***AROS*** dan ***ACOS*** 
[CAKE ACL](https://book.cakephp.org/2.0/en/core-libraries/components/access-control-lists.html). 
2. Setiap Create Controller dan Action harap daftarkan dengan mengunakan CLI Cake perintah : `bin\cake acl_extras aco_sync`
3. Aco_sync akan memabaca semua controller dan action pada project z-webadmin maka disarankan apabila menggukan plugin tambahan harap diupdate pada parent plugin di acos tersebut `status : 1`.
4. Beri nama acos pada field name agar acos bisa terdefine di menu konfigurasi hak akses.
5. Aco akan muncul di menu Groups configure.
----------
### Default Akses 
Default prefix **Z-WEBadmin** menggunakan prefix /webadmin 
ex :localhost/eueproject/webadmin
username : administrator
password : administrator